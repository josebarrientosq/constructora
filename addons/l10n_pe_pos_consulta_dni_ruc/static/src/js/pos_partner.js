odoo.define('l10n_pe_pos_consulta_dni_ruc.pos_bus_restaurant', ["web.core", 'point_of_sale.screens', 'point_of_sale.gui', "web.rpc"], function(require) {
    "use strict";
    var core = require('web.core');
    var screens = require('point_of_sale.screens');
    var gui = require('point_of_sale.gui');
    //var Model = require('web.Model');
    var rpc = require("web.rpc");

    screens.ClientListScreenWidget.include({
        rucValido: function(ruc) {
            var ex_regular_ruc;
            ex_regular_ruc = /^\d{11}(?:[-\s]\d{4})?$/;
            if (ex_regular_ruc.test(ruc)) {
                return true
            }
            return false;
        },
        dniValido: function(dni) {
            var ex_regular_dni;
            ex_regular_dni = /^\d{8}(?:[-\s]\d{4})?$/;
            if (ex_regular_dni.test(dni)) {
                return true
            }
            return false;
        },
        save_client_details: function(partner) {
            var self = this;

            var fields = {};
            this.$('.client-details-contents .detail').each(function(idx, el) {
                fields[el.name] = el.value || false;
            });

            if (!fields.name) {
                this.gui.show_popup('error', _t('A Customer Name Is Required'));
                return;
            }

            if (this.uploaded_picture) {
                fields.image = this.uploaded_picture;
            }

            fields.id = partner.id || false;
            fields.country_id = fields.country_id || false;
            /* Tipo de Documento
                2: DNI
                4: RUC
            */
            fields.catalog_06_id = fields.tipo_doc == '1' ? 2 : (fields.tipo_doc == '6' ? 4 : 7);

            if (fields.tipo_doc == '1') {
                fields.catalog_06_id = 2
                if (fields.vat) {
                    if (!self.dniValido(fields.vat)) {
                        self.gui.show_popup('error', {
                            'title': 'Error',
                            'body': "El DNI ingresado es incorrecto",
                        });
                    }
                } else {
                    self.gui.show_popup('error', {
                        'title': 'Error',
                        'body': "Si coloca que el tipo de documento es DNI, ústed debe completar el campo Documento con el DNI del Cliente.",
                    });
                }
            } else if (fields.tipo_doc == '6') {
                fields.catalog_06_id = 4
                if (fields.vat) {
                    if (!self.rucValido(fields.vat)) {
                        self.gui.show_popup('error', {
                            'title': 'Error',
                            'body': "El RUC ingresado es incorrecto",
                        });
                    }
                } else {
                    self.gui.show_popup('error', {
                        'title': 'Error',
                        'body': "Si coloca que el tipo de documento es RUC, ústed debe completar el campo Documento con el DNI del Cliente.",
                    });
                }
            } else {
                fields.catalog_06_id = 7
                if (fields.vat == "") {
                    fields.vat = "-"
                }
            }
            rpc.query({
                    model: "res.partner",
                    method: "create_from_ui",
                    args: [fields]
                })
                .then(function(partner_id) {
                    self.saved_client_details(partner_id);
                }, function(err, event) {
                    event.preventDefault();
                    var error_body = _t('Your Internet connection is probably down.');
                    if (err.data) {
                        var except = err.data;
                        error_body = except.arguments && except.arguments[0] || except.message || error_body;
                    }
                    self.gui.show_popup('error', {
                        'title': _t('Error: Could not Save Changes'),
                        'body': error_body,
                    });
                });
        },
        get_datos: function(partner, contents) {
            var self = this;
            var tipo_doc = $('.detail.tipo_doc').val();
            // Si es otro tipo de doc. que no sea dni o ruc ya no consulta.
            if (tipo_doc != '-') {
                var fields = {};
                var contents = this.$('.client-details-contents');

                this.$('.detail.vat').each(function(idx, el) {
                    fields[el.name] = el.value || false;
                });
                if (!fields.vat) {
                    this.gui.show_popup('error', {
                        'title': 'Alerta!',
                        'body': 'Ingrese nro. documento',
                    });
                    return;
                };

                rpc.query({
                    model: "res.partner",
                    method: "consulta_datos",
                    args: [tipo_doc == "1" ? "dni" : "ruc", fields.vat]
                }).then(function(datos) {
                    if (datos.error) {
                        self.gui.show_popup('error', {
                            'title': 'Alerta!',
                            'body': datos.message,
                        });
                    } else {
                        if (tipo_doc === '1') {
                            contents.find('input[name="name"]').val(datos.data.nombres + ' ' + datos.data.ape_paterno + ' ' + datos.data.ape_materno)
                        } else if (tipo_doc === '6') {
                            contents.find('input[name="name"]').val(datos.data.nombre)
                            contents.find('input[name="street"]').val(datos.data.domicilio_fiscal)
                            contents.find('input[name="city"]').val(datos.data.provincia)
                        }
                    }
                });
            }
        },
        display_client_details: function(visibility, partner, clickpos) {
            this._super(visibility, partner, clickpos);
            var self = this
            var contents = this.$('.client-details-contents');
            contents.off('click', '.button.consulta-datos');
            contents.on('click', '.button.consulta-datos', function() { self.get_datos(partner, contents); });
        },
    });


});