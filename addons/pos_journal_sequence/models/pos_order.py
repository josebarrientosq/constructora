# -*- coding: utf-8 -*-

from odoo import api, fields, models, _

class PosOrder(models.Model):
    _inherit = "pos.order"
    
    number = fields.Char(string='Number', readonly=True, copy=False)
    sequence_number = fields.Integer(string='Sequence Number', readonly=True, copy=False)
    invoice_journal = fields.Many2one('account.journal', string='Invoice Journal', readonly=True)

    def _list_invoice_type(self):
        catalogs=self.env["einvoice.catalog.01"].search([])
        list=[]
        for cat in catalogs:
            list.append((cat.code,cat.name))
        return list

    #invoice_type_code_id = fields.Selection(string="Tipo de Documento",selection=_list_invoice_type)

    def _prepare_invoice(self):
        res = super(PosOrder, self)._prepare_invoice()
        res['move_name'] = self.number
        res['journal_id']  = self.invoice_journal.id #or self.session_id.config_id.invoice_journal_id.id
        #res['invoice_type_code_id'] = self.invoice_type_code_id

        if self.invoice_journal and self.sequence_number:
            self.invoice_journal.sequence_id.pos_next(self.sequence_number+1) 
        return res
    
    @api.model
    def _order_fields(self, ui_order):
        res = super(PosOrder, self)._order_fields(ui_order)
        res['number']=ui_order.get('number', False)
        res['invoice_journal']=ui_order.get('invoice_journal', False)
        res['sequence_number']=ui_order.get('sequence_number', 0)
        #res['invoice_type_code_id']=ui_order.get('invoice_type_code_id', False)
        return res
    
    @api.model
    def create_from_ui(self, orders):
        for i, order in enumerate(orders):
            if order.get('data', {}).get('invoice_journal'): #and order.get('data', {}).get('invoice_type_code_id'):
                orders[i]['to_invoice']=True
        objs = super(PosOrder, self).create_from_ui(orders)
        arr=[]
        """
        for o in objs:
            digestvalue=self.env["pos.order"].browse(o).invoice_id.digestvalue
            arr.append([o,digestvalue])
        """
        return arr
    