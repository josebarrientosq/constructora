odoo.define('pos_journal_sequence.pos_new_ticket', ['web.core', 'point_of_sale.screens'], function(require) {
    "use strict";
    var core = require('web.core');
    var QWeb = core.qweb;
    var screens = require('point_of_sale.screens');
    //var rpc = require("web.rpc")
    //var Model = require('web.Model')
    //var ReceiptScreenWidget = screens.ReceiptScreenWidget;
    //var ReceiptScreenWidgetSuper = ReceiptScreenWidget;

    screens.ReceiptScreenWidget.prototype.render_receipt = (function() {
        var order = this.pos.get_order();
        var client = order.get_client();
        var receipt = order.export_for_printing();
        var journal = this.pos.db.get_journal_id(order.get_sale_journal());

        var total_letras = numeroALetras(order.get_total_with_tax(), {
            plural: ' SOLES',
            singular: ' SOL',
        });


        var self = this;

        function descuento(order) {
            var desc = 0;
            order.get_orderlines().forEach(function(orderline) {
                desc = desc + orderline.price * orderline.quantity * orderline.discount / 100
            })
            return desc
        }

        function total(order) {
            var tot = 0;
            order.get_orderlines().forEach(function(orderline) {
                tot = tot + orderline.price * orderline.quantity
            })
            return tot
        }
        var d = (new Date());
        self.$('.pos-receipt-container').html(QWeb.render('PosTicket', {
            widget: self,
            order: order,
            receipt: receipt,
            orderlines: order.get_orderlines(),
            paymentlines: order.get_paymentlines(),
            client: client,
            total_letra: total_letras,
            journal: journal,
            descuento: descuento(order),
            total: total(order),
            fecha_emision: d.getDate() + "/" + (d.getMonth() + 1) + "/" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes()
        }));
        if (journal) {
            var tipo_doc = journal.invoice_type_code_id;
            var serie = journal.code;
            var numero = order.get_number().substring(5, 14);
            //var FECHA_EMISION = order.formatted_validation_date.substring(0, 10);

            var FECHA_EMISION = d.getDate() + "/" + (d.getMonth() + 1) + "/" + d.getFullYear();
            var TIPO_DOC_REC = client.catalog_06_id[0].toString();
            //CALCULAR IMPUESTOS
            var MTO_TOTAL_IGV = 0;
            order.get_tax_details().forEach(function(element) {
                MTO_TOTAL_IGV = MTO_TOTAL_IGV + element.amount;
            });
            MTO_TOTAL_IGV = Number(Math.round(MTO_TOTAL_IGV + 'e2') + 'e-2');
            var texto_qr = self.pos.company.vat + "|" + tipo_doc + "|" + serie + "|" + numero + "|" + MTO_TOTAL_IGV.toString() + "|" + order.get_total_with_tax().toString() + "|" + FECHA_EMISION + "|" + TIPO_DOC_REC + "|" + client.vat + "|";
            //console.log(texto_qr);
            jQuery('#qrcode').qrcode({
                width: 180,
                height: 180,
                text: texto_qr
            });
        }




    });
});