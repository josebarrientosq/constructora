from odoo import models, fields

class Parametros_civil(models.Model):
    _name = "hr_planilla_civil.parametros"

    categoria = fields.Selection(selection=[("OPERARIO","OPERARIO"),("OFICIAL","OFICIAL"),("PEON","PEON")])
    jornal_basico = fields.Float('Jornal basico')
    jornal_dominical = fields.Float('Jornal dominical')
    buc_porcentaje = fields.Float('Buc %')
    buc = fields.Float('BUC')
    bonificacion_movilidad = fields.Float('Bonif. Movilidad')
    vacaciones_porcentaje = fields.Float('Vacaciones %')
    vacaciones = fields.Float('Vacaciones')
    cts_porcentaje = fields.Float('CTS %')
    cts = fields.Float('CTS')
    asignacion_escolar = fields.Float('Asignacion escolar')
    conafovicer_porcentaje = fields.Float('conafovicer')

class Parametros_civil(models.Model):
    _name = "hr_planilla_civil.gratificacion"

    categoria = fields.Selection(selection=[("OPERARIO", "OPERARIO"), ("OFICIAL", "OFICIAL"), ("PEON", "PEON")])
    tipo = fields.Selection(selection=[("JULIO", "JULIO"), ("DICIEMBRE", "DICIEMBRE")])
    gratificacion = fields.Float('Gratificacion')
    bono = fields.Float('Bono')