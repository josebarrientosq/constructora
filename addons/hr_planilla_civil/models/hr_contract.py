from odoo import api, fields, models

class Contract(models.Model):
    _inherit = 'hr.contract'

    categoria = fields.Selection(selection=[("OPERARIO","OPERARIO"),("OFICIAL","OFICIAL"),("PEON","PEON")])

    jornal_basico = fields.Float('Jornal basico')
    jornal_dominical = fields.Float('Jornal dominical')
    buc_porcentaje = fields.Float('Buc %')
    buc = fields.Float('BUC')
    bonificacion_movilidad = fields.Float('Bonif. Movilidad')
    vacaciones_porcentaje = fields.Float('Vacaciones %')
    vacaciones = fields.Float('Vacaciones')
    cts_porcentaje = fields.Float('CTS %')
    cts = fields.Float('CTS')
    asignacion_escolar = fields.Float('Asignacion escolar')
    conafovicer_porcentaje = fields.Float('conafovicer')


    @api.onchange('categoria')
    def get_parametros_civil(self):
        parametro = self.env["hr_planilla_civil.parametros"].search([("categoria", "=", self.categoria)])
        if parametro :
            self.jornal_basico = parametro.jornal_basico
            self.jornal_dominical = parametro.jornal_dominical
            self.buc_porcentaje = parametro.buc_porcentaje
            self.buc = parametro.buc
            self.bonificacion_movilidad = parametro.bonificacion_movilidad
            self.vacaciones_porcentaje = parametro.vacaciones_porcentaje
            self.vacaciones = parametro.vacaciones
            self.cts_porcentaje = parametro.cts_porcentaje
            self.cts = parametro.cts
            self.asignacion_escolar = parametro.asignacion_escolar
            self.conafovicer_porcentaje = parametro.conafovicer_porcentaje