from odoo import api, fields, models
from odoo.exceptions import ValidationError, UserError

class ProductTemplate(models.Model):
    _inherit = 'product.template'

    product_code = fields.Char(store=True, index=True,
                               related='product_variant_ids.product_code',
                               readonly=True, help='Product code')

    @api.onchange('categ_id')
    def getcode(self):

        if self.categ_id.parent_id.category_code and self.categ_id.category_code :
            codigo = self.categ_id.parent_id.category_code+self.categ_id.category_code
            codigo += str(self.categ_id.product_count+1)
            self.default_code = codigo