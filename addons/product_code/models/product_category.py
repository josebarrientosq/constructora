from openerp import api, fields, models

class ProductCategory(models.Model):
    _inherit = 'product.category'

    category_code = fields.Char(string= "Codigo de categoria",store=True, index=True,help='codigo categoria')


