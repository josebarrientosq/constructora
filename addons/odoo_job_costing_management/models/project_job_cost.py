# -*- coding: utf-8 -*-

from odoo import models, fields, api

class ProjectProject(models.Model):
    _inherit = 'project.project'
    
    type_of_construction = fields.Selection(
        [('residential','Residencial'),
        ('commercial','Comercial'),
        ('institutional','Institucional'),
        ('industrial','Industrial'),
        ('heavy_civil','Estructura Civil'),
        ('other','otros')],
        string='Tipos de construcción'
    )
    location_id = fields.Many2one(
        'res.partner',
        string='Lugar'
    )
    notes_ids = fields.One2many(
        'note.note', 
        'project_id', 
        string='Notas',
    )
    notes_count = fields.Integer(
        compute='_compute_notes_count', 
        string="Notes",
        #store=True,
    )
    description = fields.Text(
        string='Descripción del proyecto',
        required=False,
        copy=True,
    )

    cod_project= fields.Char(
        string="Codigo del proyecto",
        required=True,
    )

    fecha_ini = fields.Date(
        string="Fecha inicio",
        required=True,
    )
    fecha_fin = fields.Date(
        string="Fecha fin",
        required=True,
    )

    state = fields.Selection(
        selection=[('abierto', 'abierto'),
                   ('cerrado', 'cerrado'),
                   ],
        string="Estado",
        required=True,
        default= 'abierto' )

    @api.depends()
    def _compute_notes_count(self):
        for project in self:
            project.notes_count = len(project.notes_ids)
    
    @api.multi
    def view_notes(self):
        for rec in self:
            res = self.env.ref('odoo_job_costing_management.action_project_note_note')
            res = res.read()[0]
            res['domain'] = str([('project_id','in',rec.ids)])
        return res
