# -*- coding: utf-8 -*-

from odoo import models, fields, api
import odoo.addons.decimal_precision as dp

class MaterialPurchaseRequisitionLine(models.Model):
    _name = "material.purchase.requisition.line"
    
    requisition_id = fields.Many2one(
        'material.purchase.requisition',
        string='Requerimiento',
    )
    product_id = fields.Many2one(
        'product.product',
        string='Producto',
        required=True,
    )
#     layout_category_id = fields.Many2one(
#         'sale.layout_category',
#         string='Section',
#     )
    description = fields.Char(
        string='Descripción',
        required=True,
    )
    qty = fields.Float(
        string='Cantidad',
        default=1,
        required=True,
    )
    uom = fields.Many2one(
        'product.uom',
        string='Unidad',
        required=True,
    )
    partner_id = fields.Many2many(
        'res.partner',
        string='Proveedor',
    )
    requisition_type = fields.Selection(
        selection=[
                    ('internal','Entrada de inventario'),
                    ('purchase','Orden de compra'),
        ],
        string='Acción',
        default='purchase',
        required=True,
    )

    job_cost_id = fields.Many2one('job.costing', string='PARTIDA')
    job_cost_line_id = fields.Many2one( 'job.cost.line', string='PARTIDA LINEA')

    @api.onchange('product_id')
    def onchange_product_id(self):
        for rec in self:
            rec.description = rec.product_id.name
            rec.uom = rec.product_id.uom_id.id

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
