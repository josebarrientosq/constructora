function login(){
    console.log("login");
    var data = {
        "ruc" : $("#dni").val(),
        "password" : $("#password").val(),
    }

    $.post("/autenticando", data).done(function(response) {

        console.log(response)
        if (response =='dni'){
            $("#respuesta").html("<div class='alert alert-danger' role='alert'>El DNI es incorrecto</div>")
        }
        else if (response =='clave') {
            $("#respuesta").html("<div class='alert alert-danger' role='alert'>La clave es incorrecta</div>")
            }
            else{
            $("#principal").html(response)
            }
    })
}


function buscar_asistencia() {
    console.log("buscando asistencia");


    var nombre = document.getElementById("nom").innerText;
    var data = {
            "fecha_inicio":$("#fecha_inicio").val(),
            "fecha_fin":$("#fecha_fin").val(),
            "nombre" : nombre,

    }

    $.post("/asistencia", data).done(function(response) {
        $("#documento").html(response)
    })
}

function buscar_faltas() {
    console.log("buscando faltas");
    var data = {
            "fecha_inicio":$("#fecha_inicio").val(),
            "fecha_fin":$("#fecha_fin").val(),

    }
    $.post("/faltas", data).done(function(response) {
        $("#documento").html(response)
    })
}

$(".btn_reclamo").click(function() {
    var $row = $(this).closest("tr");    // Find the row
    var $text = $row.find(".nr").text(); // Find the text

    var $reclamo = $row.find(".reclamo").text(); // Find the text


    document.getElementById("recipient-name").value = $text;
    document.getElementById("message-text").value = $reclamo;


});

function enviar_reclamo() {
    var image_input = null;
    var file = document.getElementById("anexo").files[0];

    if (file) {
     var reader = new FileReader();
     reader.readAsDataURL(file);
     reader.onload = function(e)
         {
             image_input = e.target.result;
         }
    }
    var data = {
            "id":$("#recipient-name").val(),
            "reclamo":$("#message-text").val(),
            "anexo" : image_input,
    }

    $('.modal-backdrop').remove(); //remover fondo negro
    $('body').css({ overflow: 'visible'}); //permitir scroll

    $.post("/reclamo", data).done(function(response) {
        $("#documento").html(response)
    })

}


$(function(){


    $('form').submit(function(event){
        event.preventDefault()
        console.log("submi")
  })


});