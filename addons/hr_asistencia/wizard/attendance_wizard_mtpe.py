from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError
import datetime
from datetime import datetime,date
import os


class AttendanceWizard(models.TransientModel):
    _name = 'attendance.wizard.mtpe'
    _description = 'Attendance Wizard'
    _inherit = ['to.base']

    fecha_calculo = fields.Datetime('Fecha')

    @api.multi
    def generar(self):

        employee_ids = self.env['hr.employee'].search([('contract_id.state', "=", 'draft')])  # contratos vigentes
        activity_ids = self.env['attendance.activity'].search([])
        user_attendance_ids = self.env['user.attendance']
        # calendar = self.env['resource.calendar']
        asistencia_mtpe = self.env['asistencia.mtpe']
        os.system("echo '%s'" % ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"))
        os.system("echo '%s'" % (str(self.fecha_calculo)))
        fecha = datetime.strptime(str(self.fecha_calculo), "%Y-%m-%d %H:%M:%S")
        os.system("echo '%s'" % (fecha))

        fecha_local = self.convert_utc_time_to_tz(str(self.fecha_calculo))
        os.system("echo '%s'" % (fecha_local))
        fecha_obj = datetime.strptime(fecha_local, "%Y-%m-%d %H:%M:%S")
        os.system("echo '%s'" % (fecha_obj))
        timestampinicio= fecha_obj.replace(hour=0, minute=0, second=0, microsecond=0)
        os.system("echo '%s'" % (timestampinicio))
        timestamp_inicio_str = timestampinicio.strftime("%Y-%m-%d %H:%M:%S")
        os.system("echo '%s'" % (timestamp_inicio_str))
        timestampfin = fecha.replace(hour=23, minute=59, second=59, microsecond=0)
        timestamp_fin_str = timestampfin.strftime("%Y-%m-%d %H:%M:%S")


        fecha_calculo = self.convert_utc_time_to_tz(timestamp_inicio_str)
        os.system("echo '%s'" % (timestamp_inicio_str))
        os.system("echo '%s'" % (timestamp_fin_str))

        posicion_dia = fecha.weekday()

        for employee in employee_ids:

            if employee.contract_id.state == 'open':
                os.system("echo '%s'" % ("Empleado :"))
                os.system("echo '%s'" % (employee.name))


                for activity_id in activity_ids:
                    os.system("echo '%s'" % (activity_id.name))
                    calendar = employee.contract_id.resource_calendar_id
                    calendar_attendance = self.env['resource.calendar.attendance'].search(
                        [('calendar_id', '=', calendar.id),
                         ('dayofweek', '=', posicion_dia),
                         ('activity_id', '=', activity_id.id)])

                    if calendar_attendance:
                        entrada_calendario = str(calendar_attendance.hour_from)
                        salida_calendario = str(calendar_attendance.hour_to)
                        os.system("echo '%s'" % (str(calendar_attendance.hour_from)))
                        os.system("echo '%s'" % (str(calendar_attendance.hour_to)))
                        user_attendances = user_attendance_ids.search(
                                     [('employee_id', '=', employee.id),
                                     ('activity_id', '=', activity_id.id),
                                     ('timestamp', '>', timestamp_inicio_str),
                                     ('timestamp', '<', timestamp_fin_str),
                                     ], order='timestamp')

                        entrada = ""
                        salida = ""

                        marcas = [(4, line.id) for line in user_attendances]


                        checkin = []
                        checkout = []
                        entrada_str = ""
                        salida_str = ""
                        todaslasmarcas=""
                        for marca in user_attendances:
                            todaslasmarcas += self.fecha_a_hora_str(marca.timestamp)+" "

                            if marca.type == 'checkin':
                                checkin.append(marca)
                            if marca.type == 'checkout':
                                checkout.append(marca)

                        num_checkin = len(checkin)
                        num_checkout = len(checkout)

                        if num_checkin>0:
                            entrada = checkin[0].timestamp
                            entrada_str = self.fecha_a_hora_str(entrada)
                        if num_checkout>0:
                            salida = checkout[num_checkout-1].timestamp
                            salida_str = self.fecha_a_hora_str(salida)


                        os.system("echo '%s'" % (".........."))
                        os.system("echo '%s'" % (entrada))
                        os.system("echo '%s'" % (entrada_str))
                        os.system("echo '%s'" % (salida))
                        os.system("echo '%s'" % (salida_str))
                        asistencia_mtpe.create({
                            'fecha': fecha_calculo,
                            'employee_id': employee.id,
                            'activity_id': activity_id.id,

                            'user_attendance_ids': marcas,
                            'entrada': entrada,
                            'salida': salida,

                        })

                    else:
                        os.system("echo '%s'" % ("no tiene horario ese dia"))

    def fecha_a_hora_str(self,fecha):
        fecha_local = self.convert_utc_time_to_tz(fecha)
        fecha_obj = datetime.strptime(fecha_local, "%Y-%m-%d %H:%M:%S")
        return fecha_obj.strftime("%H:%M")

    def get_marcas(self):
        asistencia_mtpe = self.env['asistencia.mtpe'].search([()])
        for asistencia in asistencia_mtpe:
            asistencia.get_marcas()

    def crear_asistencia(self):
        asistencia_mtpe = self.env['asistencia.mtpe'].search([('falta', '=', True) ])
        for asistencia in asistencia_mtpe:
            asistencia.crear_asistencia()

    def crear_inasistencia(self):
        asistencia_mtpe = self.env['asistencia.mtpe'].search([('falta', '=', True) ])
        for asistencia in asistencia_mtpe:
            asistencia.crear_inasistencia()

#lines = [(0, 0, line) for line in self._get_payslip_lines(contract_ids, payslip.id)]
