# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request
import werkzeug.utils
from odoo.addons.web.controllers.main import Database
import unicodedata


class consulta_cpe(http.Controller):

    fecha_inicio= None
    fecha_fin = None
    nombre = None
    dni=None

    @http.route('/docente', auth='public')
    def login(self, **kwargs):
        return request.render('hr_asistencia.login')

    @http.route('/autenticando', type='http', auth='public', method=["POST"], csrf=False , website=True)

    def autenticar(self, **post):
        dni = post.get("dni")
        password = post.get("password")

        empleado= request.env["hr.employee"].sudo().search([('barcode','=',password)])


        if empleado :
            nombre = empleado.name
            if empleado.barcode == password:
                return http.request.render('hr_asistencia.form',{'nombre' : nombre , 'dni' : dni})
            else:
                return 'clave'
        else:
            return 'dni'

    @http.route('/asistencia', type='http', auth='public', method=["POST"], csrf=False)
    def get_asistencia(self, **post):
        self.fecha_inicio=post.get("fecha_inicio")
        self.fecha_fin = post.get("fecha_fin")
        self.nombre = post.get("nombre")

        asistencia = request.env["hr.attendance"].sudo().search([('check_in','>=', self.fecha_inicio),('check_in','<=',self.fecha_fin),('employee_id.name', '=', self.nombre)])
        #account_invoice_log = documento.account_log_status_ids[len(documento.account_log_status_ids)-1]
        identificador = ""
        nombre = ""
        #if account_invoice_log.exists():
        #    identificador = account_invoice_log.api_request_id
        #    nombre = account_invoice_log.name

        return request.render('hr_asistencia.asistencia',{'asistencia' : asistencia,"identificador":identificador,"nombre":nombre})

    @http.route('/faltas', type='http', auth='public', method=["POST"], csrf=False)
    def get_faltas(self, **post):
        self.fecha_inicio=post.get("fecha_inicio")
        self.fecha_fin = post.get("fecha_fin")
        falta = request.env["hr.holidays"].sudo().search([('date_from','>=', self.fecha_inicio),('date_from','<=',self.fecha_fin)])
        #account_invoice_log = documento.account_log_status_ids[len(documento.account_log_status_ids)-1]
        identificador = ""
        nombre = ""
        #if account_invoice_log.exists():
        #    identificador = account_invoice_log.api_request_id
        #    nombre = account_invoice_log.name

        return request.render('hr_asistencia.faltas',{'falta' : falta,"identificador":identificador,"nombre":nombre})



    @http.route('/reclamo', type='http', auth='public', method=["POST"], csrf=False)
    def reclamo(self, **post):
        id=post.get("id")
        anexo = post.get("anexo")
        article_1 = unicodedata.normalize('NFKD', anexo).encode('ascii', 'ignore')
        article_2 = article_1.lstrip('data:image/jpeg;base64,')

        asistencia = request.env["hr.attendance"].sudo().search([('id', '=', id) ])
        asistencia.reclamo = post.get("reclamo")
        asistencia.reclamo = '/' + article_2

        identificador = ""
        nombre = ""

        asistencia= request.env["hr.attendance"].sudo().search([('check_in','>=',self.fecha_inicio),('check_in','<=',self.fecha_fin)])
        return request.render('hr_asistencia.asistencia',{'asistencia' : asistencia,"identificador":identificador,"nombre":nombre})

    @http.route("/consulta/comprobante/xml/<identificador>", type='http', method=["GET"],csrf=False)
    def comprobante_xml(self,identificador):
        log = request.env["account.log.status"].search([["api_request_id","=",identificador]])
        xml = log.signed_xml_data
        xml_headers = [
            ('Content-Type', 'application/xml'),
            ('Content-Length', len(xml)),
            ('Content-Disposition', 'attachment; filename="%s.xml"'%(log.name)),
        ]
        return request.make_response(xml, headers=xml_headers)

    @http.route("/consulta/comprobante/pdf/<identificador>",type='http',method=["GET"],csrf=False)
    def comprobante_pdf(self,identificador):
        log = request.env["account.log.status"].search([["api_request_id","=",identificador]])
        invoice = log.account_invoice_id
        if invoice:
            pdf = request.env.ref('account.account_invoices_without_payment').sudo().render_qweb_pdf([invoice.id])[0]
            pdfhttpheaders = [('Content-Type', 'application/pdf'),
                                ('Content-Length', len(pdf)),
                                ('Content-Disposition', 'attachment; filename="%s.pdf"'%(log.name))]
            return request.make_response(pdf, headers=pdfhttpheaders)
        else:
            return request.redirect('/')


        #    ['|', ["vat", "=", identificador], ["codigo", "=", identificador], ["es_comensal", "=", True]])

#     @http.route('/json/json/objects/', auth='public')
#     def list(self, **kw):
#         return http.request.render('json.listing', {
#             'root': '/json/json',
#             'objects': http.request.env['json.json'].search([]),
#         })

#     @http.route('/json/json/objects/<model("json.json"):obj>/', auth='public')
#     def object(self, obj, **kw):
#         return http.request.render('json.object', {
#             'object': obj
#         })