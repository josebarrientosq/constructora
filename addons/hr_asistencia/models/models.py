# -*- coding: utf-8 -*-

import datetime
from datetime import datetime,date
from pytz import timezone
from odoo import models, fields, api, exceptions,_
from odoo.exceptions import UserError,ValidationError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
import logging,os
_logger = logging.getLogger(__name__)

def totalminutos(datetime):
    a = datetime.time()
    f = round(a.hour * 60 + a.minute) -300
    return f

def totalhoras(datetime):
    a = datetime.time()
    return round(a.hour)

def difpositivo(num):
    if(num<0):
        return 0
    else:
        return num

""""
class HrAttendance(models.Model):
    _inherit = ['hr.attendance']

    tardanza = fields.Float(string='tardanza')
    reclamo = fields.Text("Reclamo")
    anexo = fields.Binary("Anexo", attachment=True)



    #@api.onchange('check_in')
    def _compute_tardanza(self):
        calendario = self.employee_id.resource_calendar_id
        if (self.check_in):
            posicion_dia = datetime.strptime(self.check_in, "%Y-%m-%d %H:%M:%S").weekday()
            start_dt = datetime.strptime(self.check_in, "%Y-%m-%d %H:%M:%S")  # convert into datetime fromat


            if ( totalminutos(start_dt)>calendario.cambio_turno*60):  #si supera el cambio de turno (en minutos) devuelve 1 posicion mas
                posicion_calendario = 2*posicion_dia+1
            else:
                posicion_calendario = 2*posicion_dia

            entrada1turno = calendario.attendance_ids[posicion_calendario].hour_from

            difentrada1=difpositivo(totalminutos(start_dt)- entrada1turno*60)

            if difentrada1 < calendario.max_tardanza*60 :
                self.tardanza = difentrada1
            else :
                self.tardanza = calendario.max_tardanza*60


    def asignar_falta(self):
        falta  = self.env['hr.holidays']
        tipo_falta = self.env['hr.holidays.status'].search([('name',"=",'FALTA')])
        today = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        empleados = self.env['hr.employee'].search([('contract_id.state',"=",'open')]) #contratos vigentes
        for emp in empleados:
            falta.create({
                'name': 'FALTA POR INASISTENCIA',
                'holiday_type': 'employee',
                'holiday_status_id': tipo_falta.id,
                'date_from': today,
                'date_to': today,
                'report_notes': 'Falta',
                'number_of_days_temp': 1,
                'employee_id': emp.id
            })
            #asistencia = self.env['hr.attendance'].search([('check_in','>=',today) ,('employee_id', '=', emp.id)])

"""


class ResourceCalendar(models.Model):
    _inherit = ['resource.calendar']

    entrada1turno= fields.Float(string="hora entrada")
    salida1turno = fields.Float(string="hora salida")
    cambio_turno = fields.Float(string="cambio turno")
    max_tardanza = fields.Float(string="Tardanza maxima")





class ResourceCalendarAttendance(models.Model):
    _inherit = "resource.calendar.attendance"

    activity_id = fields.Many2one('attendance.activity', string='Attendance Activity',
                                  help='This field is to group attendance into multiple Activity (e.g. Overtime, Normal Working, etc)')



class HrPayslip(models.Model):
    _inherit = ['hr.payslip']

    @api.one
    def calcular_faltas_tardanzas(self):
        employee_id=self.employee_id
        fecha_inicio=self.date_from
        fecha_fin=self.date_to
        calendar_id=self.employee_id.contract_id.resource_calendar_id
        HrAttendance = self.env['hr.attendance']
        activity_ids = self.env['attendance.activity'].search([])

        horario_ingreso = {}

        for turno in calendar_id.attendance_ids:
            os.system("echo '%s'"%("OSSSSS"))
            os.system("echo '%s'" % (turno.dayofweek))
            os.system("echo '%s'" % (turno.activity_id.code))

            #horario_ingreso[turno.dayofweek][turno.activity_id.code] = turno.hour_from

        startDate = datetime.strptime(self.check_in, "%Y-%m-%d %H:%M:%S")
        endDate = datetime.strptime(self.check_out, "%Y-%m-%d %H:%M:%S")
        dayDelta = datetime.timedelta(days=1)

        while startDate < endDate:
            os.system("echo '%s'" % (startDate))
            startDate += dayDelta


        d = fecha_inicio
        #d.strftime("%Y-%m-%d")
        delta = datetime.timedelta(days=1)
        os.system("echo '%s'" % ((fecha_fin - fecha_inicio).days))
        for i in range((fecha_fin - fecha_inicio).days):
            d = fecha_inicio + i*delta
            for activity_id in activity_ids:
                #posicion_dia = datetime.strptime(self.check_in, "%Y-%m-%d %H:%M:%S").weekday()
                posicion_dia = datetime.strftime("%Y-%m-%d").weekday()

                hr_attendance_id = HrAttendance.search(
                    [('employee_id', '=', employee_id.id),
                     ('activity_id', 'in', (activity_id.id, False)),
                     ('check_in', '<=', d)], limit=1, order='check_in DESC')


                if hr_attendance_id:
                    start_dt = datetime.strptime(hr_attendance_id.check_in, "%Y-%m-%d %H:%M:%S")
                    difentrada1 = difpositivo(totalminutos(start_dt) - horario_ingreso[posicion_dia][hr_attendance_id.acivity_id.code] * 60)
                    hr_attendance_id.write({'tardanza': difentrada1})

                d = d + delta

    def calcular(self):
        employee_ids = self.env['hr.employee'].search([('contract_id.state',"=",'draft')]) #contratos vigentes
        activity_ids = self.env['attendance.activity'].search([])
        HrAttendance = self.env['hr.attendance']
        #calendar = self.env['resource.calendar']

        dt = datetime.now(timezone('UTC'))
        dt = dt.astimezone(timezone('America/Lima'))
        today = dt.replace(hour=0, minute=0, second=0, microsecond=0)
        timestampStr = today.strftime("%Y-%m-%d %H:%M:%S")


        os.system("echo '%s'" % (timestampStr))
        posicion_dia = datetime.now().weekday()


        falta = self.env['hr.holidays']
        tipo_falta = self.env['hr.holidays.status'].search([('name', "=", 'FALTA')])

        for employee in employee_ids:

            if employee.contract_id.state == 'open':
                os.system("echo '%s'" % ("Empleado :"))
                os.system("echo '%s'" % (employee.name))

                calendar = employee.contract_id.resource_calendar_id

                for activity_id in activity_ids:
                    os.system("echo '%s'" % (activity_id.name))
                    calendar_attendance = self.env['resource.calendar.attendance'].search(
                        [('calendar_id', '=', calendar.id),
                         ('dayofweek', '=', posicion_dia),
                         ('activity_id', '=', activity_id.id)])
                    if calendar_attendance:
                        os.system("echo '%s'" % ("encontro horario"))
                        hora_entrada = calendar_attendance.hour_from
                        os.system("echo '%s'" % (calendar_attendance.hour_from))

                        os.system("echo '%s'" % ("buscar asistencia"))
                        os.system("echo '%s'" % (timestampStr))
                        hr_attendance_id = HrAttendance.search(
                            [('employee_id', '=', employee.id),
                             ('activity_id', '=', activity_id.id),
                             ('check_in', '>', timestampStr)], limit=1, order='check_in DESC')

                        if hr_attendance_id:
                            os.system("echo '%s'" % ("encontro asistencia"))
                            start_dt = datetime.strptime(hr_attendance_id.check_in, "%Y-%m-%d %H:%M:%S")
                            os.system("echo '%s'" % (start_dt))
                            difentrada1 = difpositivo(totalminutos(start_dt) - hora_entrada * 60)
                            os.system("echo '%s'" % (difentrada1))
                            hr_attendance_id.write({'tardanza': difentrada1})

                        else:
                            os.system("echo '%s'" % ("no hay asistencia"))
                            if activity_id.code == 0:
                                os.system("echo '%s'" % ("crear falta"))
                                os.system("echo '%s'" % (today))
                                falta.create({
                                    'name': 'falta no justificada',
                                    'holiday_type': 'employee',
                                    'holiday_status_id': tipo_falta.id,
                                    'date_from': today,
                                    'date_to': today,
                                    'report_note': 'Falta',
                                    'number_of_days_temp': 1,
                                    'employee_id': employee.id
                                })

                    else:
                        os.system("echo '%s'" % ("no tiene horario ese dia"))



    @api.one
    def compute_tardanza(self):
        work_day = self.env['hr.payslip.worked_days']

        asistencias = self.env['hr.attendance'].search([('check_in',">=",self.date_from),('check_out',"<=",self.date_to),('employee_id','=',self.employee_id.id)])
        tardanza=0
        for asistencia in asistencias:
            tardanza += asistencia.tardanza

        for li in self.browse(self.ids):
            tardanza_dict={
                'payslip_id': li.id,
                'name': 'Tardanzas por minuto',
                'sequence': 2,
                'code': 'TARDANZA',
                'number_of_days': '',
                'number_of_hours': '',
                'number_of_min': tardanza,
                'contract_id': '1'

            }

            for work in li.worked_days_line_ids:
                if work.code == 'TARDANZA':
                    work.unlink()

            work_day.create(tardanza_dict)

class HrPayslipRun(models.Model):
    _inherit = 'hr.payslip.run'

    @api.one
    def generar_tardanzas(self):
        for planilla in self.slip_ids:
            planilla.compute_tardanza()