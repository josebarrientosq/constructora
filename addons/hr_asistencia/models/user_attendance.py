from odoo import models, fields, api, _
import datetime
from datetime import datetime,date

class UserAttendance(models.Model):
    _inherit = 'user.attendance'

    observacion = fields.Char("Observacion")
    asistencia_mtpe_id = fields.Many2one('asistencia.mtpe', string='mtpe')

    @api.model
    def is_valid(self):
        if not self.employee_id:
            return False

        start_dt = datetime.strptime(self.timestamp, "%Y-%m-%d %H:%M:%S")
        timestampStr= start_dt.replace(hour=0, minute=0, second=0, microsecond=0)
        timestamp_inicio_str = timestampStr.strftime("%Y-%m-%d %H:%M:%S")



        if self.type == 'checkin':

            prev_att = self.search([('employee_id', '=', self.employee_id.id),
                                    ('timestamp', '<', self.timestamp),
                                    ('timestamp', '>', timestamp_inicio_str),
                                    ('attendance_state_id', '=', self.attendance_state_id.id)], limit=1, order='timestamp DESC')

            if not prev_att:
                valid = self.type == 'checkin' and True or False

                if not valid:
                    self.observacion = "no hay checkin"
                else:
                    self.observacion = "checkin ok"

            else :
                valid = prev_att.type != self.attendance_state_id.type and True or False
                if not valid:
                    self.observacion = "checkin repetido"
                else:
                    self.observacion = "checkin ok"

        if self.type == 'checkout':
            sig_att = self.search([('employee_id', '=', self.employee_id.id),
                                    ('timestamp', '>', self.timestamp),
                                    ('attendance_state_id', '=', self.attendance_state_id.id)], limit=1, order='timestamp DESC')
            valid = sig_att.type != self.attendance_state_id.type and True or False
            if not valid:
                self.observacion = "chekout repetido"
            else:
                self.observacion = "checkout ok"


        return valid
