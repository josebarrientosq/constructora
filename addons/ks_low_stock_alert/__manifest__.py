{
    'name': "Low Stock Alert",

    'summary': """
        Low Stock Alert v12.0""",

    'description': """
              Low Stock Alert for odoo 12.0. 
      """,
    'author': "Ksolves",
    'website': "https://www.ksolves.com/",
    'license': 'OPL-1',
    'currency': 'EUR',
    'price': 9.0,
    'live_test_url': 'https://youtu.be/qTROQ6HTK1E',
    'category': 'Tools',
    'support': 'sales@ksolves.com',
    'version': '1.0.1',
    'images': [
        'static/description/main.jpg',
    ],
    # any module necessary for this one to work correctly
    'depends': ['base','web','stock'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/assets.xml',

    ],
}
# -*- coding: utf-8 -*-
