from odoo import models, fields, api

class ks_check_product_quantity(models.Model):
    _inherit = 'product.template'
    is_product_quantity_avail= fields.Integer(compute='ks_check_prod_quantity')
    warehouses_names = fields.Char(compute='ks_get_warehosues_with_low_stock')

    #Compute function for the to check if product quantity is low at any location
    def ks_check_prod_quantity(self):
        for rec in self:
            is_order_ponit_obj = rec.env['stock.warehouse.orderpoint'].search([("product_id",'=',rec.id)],limit=1)
            order_ponit_obj = rec.env['stock.warehouse.orderpoint'].search([("product_id",'=',rec.id),("product_min_qty",">=",rec.qty_available)],limit=1)
            if not is_order_ponit_obj.product_id.id:
                rec.is_product_quantity_avail = 100
            elif not order_ponit_obj.product_id.id:
                rec.is_product_quantity_avail = 300
            else:
                rec.is_product_quantity_avail = 200

    # Override read fucntion to add is_product_quantity_avail so it can be used in javascript
    def read(self, records,load='_classic_read'):
        if 'is_product_quantity_avail' not in records:
            records.append("is_product_quantity_avail")
            records.append("warehouses_names")
        return super(ks_check_product_quantity, self).read(records,load)


    # Getting all the warehouses names where product quantity is low
    def ks_get_warehosues_with_low_stock(self):
        warehouses_name = []
        for rec in self:
            warehosue_ids = rec.env['stock.warehouse.orderpoint'].search(
                [("product_id", '=', rec.id), ("product_min_qty", ">=", rec.qty_available)]).mapped("warehouse_id")
            for ids in warehosue_ids:
                warehouses_name.append(rec.env['stock.warehouse'].search([('id','=',ids.id)]).name)
            rec.warehouses_names = str(warehouses_name)
            warehouses_name.clear()
