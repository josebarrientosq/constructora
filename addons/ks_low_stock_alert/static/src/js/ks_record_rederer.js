odoo.define('ks_low_stock_alert.ks_record_rederer',function(require){
    "use strict";
    var ks_record_rederer = require('web.KanbanRecord');
    ks_record_rederer.include({
        _setupColor:function(fragment){
               if(typeof(this.recordData.qty_available) != 'undefined'){
                if(this.recordData.is_product_quantity_avail===200){
                     $(this.el).css("background-color","rgb(165, 119, 127) !important");
                     $(this.el).css("color","white");
                     $(this.el).css("font-weight","bold");
                     $(this.el).attr('title', "Cantidad por debajo en \n @"+this.recordData.warehouses_names);

                }
                else if(this.recordData.is_product_quantity_avail===100){
                     $(this.el).css("background-color","rgba(117, 116, 173, 0.95) !important");
                     $(this.el).css("color","white");
                     $(this.el).css("font-weight","bold");
                     $(this.el).attr('title', "Establece el stock mínimo");
                    }
                else{
                      this._super(fragment);
                }
            }
            else{
                this._super(fragment);
            }


        }

    })
});