odoo.define('ks_low_stock_alert.ks_listview_rederer',function(require){
    "use strict";
    var ks_ListView_rederer = require('web.ListRenderer');
    ks_ListView_rederer.include({
       _setDecorationClasses: function (record, $tr) {
            if(typeof(record.data.qty_available) != 'undefined'){
                if(record.data.is_product_quantity_avail===200){
                   $tr.css("background-color","rgb(165, 119, 127) !important");
                   $tr.css("font-weight", "bold");
                   $tr.css("color", "azure");
                   $tr.attr('title', "Cantidad por debajo en \n @"+record.data.warehouses_names);

                }
                else if(record.data.is_product_quantity_avail===100){
                   $tr.css("background-color","rgba(117, 116, 173, 0.95) !important");
                   $tr.css("font-weight", "bold");
                   $tr.css("color", "azure");
                   $tr.attr('title', "Establece el stock mínimo");
                }
                else{
                      this._super(record, $tr);
                }
            }
            else{
                this._super(record, $tr);
            }
        }
    })
});