# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request
import werkzeug.utils
from odoo.addons.web.controllers.main import Database
import unicodedata
import os

class consulta_cpe(http.Controller):

    fecha_inicio= None
    fecha_fin = None
    nombre = None
    dni=None

    @http.route('/planilla', auth='public')
    def login(self, **kwargs):
        return request.render('hr_planilla.login')


    @http.route('/autenticador', type='http', auth='public', method=["POST"], csrf=False , website=True)
    def autenticar(self, **post):
        dni = post.get("dni")
        password = post.get("password")

        empleado= request.env["hr.employee"].sudo().search([('identification_id','=',dni)])

        if empleado:

            nombre = empleado.name
            if empleado.clave == password or True:
                return http.request.render('hr_planilla.principal', {'nombre': nombre, 'dni': dni})
            else:
                return 'clave'
        else:
            return 'dni'


    @http.route('/mostrarplanilla', type='http', auth='public', method=["POST"], csrf=False)
    def mostrar_planillas(self, **post):
        dni = post.get("dni")

        empleado = request.env["hr.employee"].sudo().search([('identification_id','=',dni)], limit=1)

        planilla = request.env["hr.payslip"].sudo().search([ ('employee_id', '=', empleado.id)])

        return request.render('hr_planilla.boletas_pago', {'documento': planilla})

    @http.route("/consulta/planilla/pdf/<identificador>", auth='public', type='http', method=["GET"], csrf=False)
    def get_pdf(self, identificador):

        planilla = request.env["hr.payslip"].sudo().search([ ('number', '=', identificador)])
        os.system("echo '%s'" % (planilla))
        if planilla:
            pdf = request.env.ref('hr_planilla.planilla_detalle_report').sudo().render_qweb_pdf([planilla.id])[0]
            pdfhttpheaders = [('Content-Type', 'application/pdf'),
                              ('Content-Length', len(pdf)),
                              ('Content-Disposition', 'attachment; filename="%s.pdf"' % (planilla.number))]
            return request.make_response(pdf, headers=pdfhttpheaders)
        else:
            return request.redirect('/')