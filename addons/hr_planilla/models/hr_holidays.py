from odoo import api, fields, models

class HolidaysType(models.Model):
    _inherit = "hr.holidays.status"

    codigo = fields.Char()
    suspension = fields.Selection([
        ('SP', 'SUSPENSION PERFECTA'),
        ('SI', 'SUSPENSION IMPERFECTA'),
        ], string='Suspensión')


class Holidays(models.Model):
    _inherit = "hr.holidays"

    justificado = fields.Boolean('Justificado')