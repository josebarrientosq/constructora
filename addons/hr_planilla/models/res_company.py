from odoo import api,models,fields,_


class ResCompany(models.Model):
    _inherit = "res.company"

    firma_gerente = fields.Binary('Firma Gerente')
    sello_empresa = fields.Binary('Sello Empresa')

