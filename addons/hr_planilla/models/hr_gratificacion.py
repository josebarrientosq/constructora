from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
import os

from datetime import datetime, date, timedelta


class Gratificaciones(models.Model):
    _name = 'hr_planilla.gratificaciones'
    _description = 'Gratificaciones'


    empleado_id = fields.Many2one('hr.employee')
    period_id = fields.Many2one('account.period', "Periodo", required=True)

    anio = fields.Integer('Año')
    tipo = fields.Selection(selection=[("FP","FIESTAS PATRIAS"),("NV","NAVIDAD"),("TR","TRUNCA")])

    fecha_ingreso = fields.Date('Fecha de ingreso', readonly=True)
    fecha_cese = fields.Date('Fecha de cese', readonly=True)
    periodos_computables = fields.Integer('Periodos computables', readonly=True) #entre 1 y 6
    dias_computables = fields.Integer('dias computables', readonly=True) #entre 1 y 30

    conceptos_fijos = fields.Float('conceptos fijos')
    conceptos_variables = fields.Float('conceptos variables')
    remuneracion_computable = fields.Float('remuneracion computable', readonly=True)
    gratificacion_completa = fields.Float('Gratificación Completa', readonly=True)
    dias_inasistencia = fields.Integer('dias de inasistencia',default=0)
    gratificacion_neta = fields.Float('Gratificación Neta', readonly=True)
    bonificacion_extraordinaria = fields.Float('Bonificacion extraordinaria',readonly=True)
    monto_recibir = fields.Float('Monto a recibir', readonly=True)

    gratificacion_run_id = fields.Many2one('hr_planilla.gratificacion.run')


    def calcular_periodos_computables(self,mes):
        self.fecha_ingreso = self.empleado_id.contract_id.date_start
        self.fecha_cese = self.empleado_id.contract_id.date_end


        if self.tipo =='FP':
            primer_mes = 1 # enero
            ultimo_dia = 30# junio
            ultimo_mes = 6# junio

        if self.tipo == 'NV':
            primer_mes = 7  # julio
            ultimo_dia = 31  # dic
            ultimo_mes = 12  # dic

        if self.tipo == 'TR':
            if mes>=1 and mes <=6:
                primer_mes = 1  # enero
                ultimo_dia = 30  # junio
                ultimo_mes = 6  # junio
            else:
                primer_mes = 7  # julio
                ultimo_dia = 31  # dic
                ultimo_mes = 12  # dic

        # se define los limites del calculo

        if self.fecha_ingreso:
            fecha_ingreso = datetime.strptime(str(self.fecha_ingreso), "%Y-%m-%d").date()
            if fecha_ingreso < date(self.anio, primer_mes, 1): #1-1-2019 o 1-7-2019
                fecha_inicio_calculo = date(self.anio, primer_mes, 1)
            else:
                fecha_inicio_calculo = fecha_ingreso
        else :
            raise UserError("Falta fecha inicio de labores")

        if self.fecha_cese:
            fecha_cese = datetime.strptime(str(self.fecha_cese), "%Y-%m-%d").date()
            if fecha_cese < date(self.anio, ultimo_mes, ultimo_dia):
                fecha_cese_calculo = fecha_cese
            else:
                fecha_cese_calculo = date(self.anio, ultimo_mes, ultimo_dia)
        else:
            fecha_cese_calculo = date(self.anio, ultimo_mes, ultimo_dia) #30 - 06 - 2019 o 31-12


        #se establece los meses y dias computables para el calculo

        if fecha_inicio_calculo.day == 1:
            mes_inicio = fecha_inicio_calculo.month
            dia_inicio = 0
        else :
            mes_inicio = fecha_inicio_calculo.month + 1
            dia_inicio = last_day_of_month(fecha_inicio_calculo).day - fecha_inicio_calculo.day +1

        if fecha_cese_calculo.day == 30 or fecha_cese_calculo.day == 31 or fecha_cese_calculo.day == 29 or fecha_cese_calculo.day == 28:
            if fecha_cese_calculo.month==1: #febrero
                mes_final = 1
            else :
                mes_final = fecha_cese_calculo.month
            dia_fin = 0
        else:
            mes_final = fecha_cese_calculo.month-1
            dia_fin = fecha_cese_calculo.day

        self.periodos_computables= mes_final - mes_inicio + 1
        self.dias_computables = dia_inicio + dia_fin

    def calcular_gratificacion(self,mes):
        self.calcular_periodos_computables(mes)
        self.remuneracion_computable = self.conceptos_fijos + self.conceptos_variables

        #self.gratificacion_completa = self.remuneracion_computable / 6 *( self.periodos_computables + self.dias_computables/30)
        #self.gratificacion_neta = self.gratificacion_completa - self.dias_inasistencia

        monto_mes = self.remuneracion_computable * self.periodos_computables / 6
        monto_dia = self.remuneracion_computable * self.dias_computables / 180
        self.gratificacion_completa = monto_mes + monto_dia

        monto_inasistencia = self.remuneracion_computable * self.dias_inasistencia / 180

        self.gratificacion_neta = self.gratificacion_completa - monto_inasistencia


        tasa = self.empleado_id.aportes.tasa/100
        self.bonificacion_extraordinaria = self.gratificacion_neta * tasa
        self.monto_recibir = self.gratificacion_neta + self.bonificacion_extraordinaria


def last_day_of_month(any_day):
    next_month = any_day.replace(day=28) + timedelta(days=4)  # this will never fail
    return next_month - timedelta(days=next_month.day)


class GratificacionRun(models.Model):
    _name = 'hr_planilla.gratificacion.run'

    name = fields.Char(required=True)
    period_id = fields.Many2one('account.period', "Periodo", required=True)
    tipo = fields.Selection(selection=[("FP", "FIESTAS PATRIAS"), ("NV", "NAVIDAD"),("TR", "TRUNCA")],required=True)

    empleados_ids = fields.Many2many('hr.employee',string='Empleados' , required=True)
    gratificacion_ids = fields.One2many('hr_planilla.gratificaciones', 'gratificacion_run_id', string='linea gratificacion')


    def calcular_gratificaciones(self):

        if not self.empleados_ids:
            raise UserError("Seleccione al menos un empleado")

        self.env["hr_planilla.gratificaciones"].search([("gratificacion_run_id.name", "=", self.name)]).unlink()

        periodo = str(self.period_id.code)
        anio = int(periodo[3:7])
        mes = int(periodo[0:2])

        obj_gratificaciones = self.env['hr_planilla.gratificaciones']
        obj_empleado = self.env['hr.employee'].search([('contract_id.state', '=', 'open')])

        for li in self.browse(self.ids):
            for emp in self.empleados_ids:

                gratificacion_dict = {
                    'gratificacion_run_id': li.id,
                    'period_id': self.period_id.id,
                    'empleado_id': emp.id,
                    'anio': anio,
                    'tipo': self.tipo,
                    'conceptos_fijos': float(emp.contract_id.wage) + float(emp.asig_fam)
                }

                gratificaciones = obj_gratificaciones.create(gratificacion_dict)
                gratificaciones.calcular_gratificacion(mes)
