from odoo import api, fields, models


class HrEmployee(models.Model):
    _inherit = 'hr.employee'

    afiliacion = fields.Many2one('hr_planilla.afiliaciones')
    aportes = fields.Many2one('hr_planilla.aportes')
    cuspp = fields.Char(string='CUSPP')
    firma = fields.Binary("firma", attachment=True)
    asig_fam = fields.Float("Asignacion Familiar" , compute="get_asig_fam", default=0.0)
    cta_bancaria = fields.Char(string='Cta. bancaria')
    clave = fields.Char('Clave acceso',compute="get_clave")

    @api.one
    def get_asig_fam(self):
        conf = self.env['ir.config_parameter']
        if self.children >0:
            self.asig_fam = float(conf.get_param('asig_fam'))

    def get_clave(self):
        cta = self.cta_bancaria
        if cta :
            cta = str(cta).replace('-', '')
            tam = len(cta)
            self.clave = cta[tam-4:tam]