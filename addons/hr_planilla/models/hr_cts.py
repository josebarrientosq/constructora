from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
import os

from datetime import datetime, date, timedelta , time

class CTS(models.Model):
    _name = 'hr_planilla.cts'
    _description = 'CTS'


    empleado_id = fields.Many2one('hr.employee')

    period_id = fields.Many2one('account.period', "Periodo", required=True)

    anio = fields.Integer('Año')
    tipo = fields.Selection(selection=[("MY","MAYO"),("NV","NOVIEMBRE"),("TR","TRUNCA")])

    fecha_ingreso = fields.Date('Fecha de ingreso',related = 'empleado_id.contract_id.date_start', readonly=True)
    fecha_cese = fields.Date('Fecha de cese', related = 'empleado_id.contract_id.date_end' , readonly=True)

    periodos_computables = fields.Integer('Periodos computables', readonly=True) #entre 1 y 6
    dias_computables = fields.Integer('dias computables', readonly=True) #entre 1 y 30

    ingresos = fields.Float('ingresos')
    ultima_gratificacion_neta = fields.Float('Gratificación ultima')
    remuneracion_computable = fields.Float('remuneracion computable', readonly=True)
    cts_neta = fields.Float('CTS Neta', readonly=True)

    dias_inasistencia = fields.Integer('dias inasistencia', default=0)
    monto_recibir = fields.Float('Monto a recibir', readonly=True)

    cts_run_id = fields.Many2one('hr_planilla.cts.run')


    def calcular_periodos_computables(self):
        self.fecha_ingreso = self.empleado_id.contract_id.date_start
        self.fecha_cese = self.empleado_id.contract_id.date_end

        periodo = str(self.period_id.code)
        anio = int(periodo[3:7])
        mes = int(periodo[0:2])


        if self.tipo =='MY':
            primer_mes = 11 # nov
            primer_anio = self.anio-1
            ultimo_dia = 30# abril
            ultimo_mes = 4 # abril
            ultimo_anio = self.anio


        if self.tipo == 'NV':
            primer_mes = 5  # mayo
            primer_anio = self.anio
            ultimo_dia = 31  # oct
            ultimo_mes = 10  # oct
            ultimo_anio = self.anio

        if self.tipo == 'TR':
            if mes >=5 and mes <= 10:
                primer_mes = 5  # mayo
                primer_anio = self.anio
                ultimo_dia = 31  # oct
                ultimo_mes = 10  # oct
                ultimo_anio = self.anio
            else:
                if mes>=1 and mes <=4:
                    primer_mes = 11  # nov
                    primer_anio = self.anio - 1
                    ultimo_dia = 30  # abril
                    ultimo_mes = 4  # abril
                    ultimo_anio = self.anio
                else :
                    primer_mes = 11  # nov
                    primer_anio = self.anio
                    ultimo_dia = 30  # abril
                    ultimo_mes = 4  # abril
                    ultimo_anio = self.anio+1

        if self.fecha_ingreso:
            fecha_ingreso = datetime.strptime(str(self.fecha_ingreso), "%Y-%m-%d").date()
            if fecha_ingreso < date(primer_anio, primer_mes, 1): #1-5-2019 o 1-11-2018
                fecha_inicio_calculo = date(primer_anio, primer_mes, 1)
            else:
                fecha_inicio_calculo = fecha_ingreso
        else :
            raise UserError("Falta fecha inicio de labores")

        if self.fecha_cese:
            fecha_cese = datetime.strptime(str(self.fecha_cese), "%Y-%m-%d").date()
            if fecha_cese < date(ultimo_anio, ultimo_mes, ultimo_dia):
                fecha_cese_calculo = fecha_cese
            else:
                fecha_cese_calculo = date(ultimo_anio, ultimo_mes, ultimo_dia)
        else:
            fecha_cese_calculo = date(ultimo_anio, ultimo_mes, ultimo_dia) #30 - 04 - 2020 o 31-10

        # se establece los meses y dias computables para el calculo

        if fecha_inicio_calculo.day == 1:
            mes_inicio = fecha_inicio_calculo.month
            dia_inicio = 0
        else :
            mes_inicio = fecha_inicio_calculo.month + 1
            dia_inicio = last_day_of_month(fecha_inicio_calculo).day - fecha_inicio_calculo.day + 1

        if fecha_cese_calculo.day == 30 or fecha_cese_calculo.day == 31 or fecha_cese_calculo.day == 29 or fecha_cese_calculo.day == 28:
            if fecha_cese_calculo.month==1: #febrero
                mes_final = 1
            else :
                mes_final = fecha_cese_calculo.month
            dia_fin = 0

        else:
            mes_final = fecha_cese_calculo.month-1
            dia_fin = fecha_cese_calculo.day

        self.periodos_computables= mes_final - mes_inicio + 1
        self.dias_computables = dia_inicio + dia_fin

    def calcular_cts(self):
        self.calcular_periodos_computables()
        self.remuneracion_computable = self.ingresos + self.ultima_gratificacion_neta/6
        #self.cts_neta = self.remuneracion_computable / 12 * (self.periodos_computables + self.dias_computables / 30)
        monto_mes = self.remuneracion_computable*self.periodos_computables /12
        monto_dia = self.remuneracion_computable*self.dias_computables/ 360
        self.cts_neta = monto_mes + monto_dia

        monto_inasistencia = self.remuneracion_computable *self.dias_inasistencia/ 360

        self.monto_recibir = self.cts_neta - monto_inasistencia

def last_day_of_month(any_day):
    next_month = any_day.replace(day=28) + timedelta(days=4)  # this will never fail
    return next_month - timedelta(days=next_month.day)


class CtsRun(models.Model):
    _name = 'hr_planilla.cts.run'

    name = fields.Char(required=True)

    period_id = fields.Many2one('account.period', "Periodo", required=True)
    tipo = fields.Selection(selection=[("MY", "MAYO"), ("NV", "NOVIEMBRE"), ("TR", "TRUNCA")],required=True)

    period_grati_id = fields.Many2one('account.period', "Periodo Gratificacion")
    tipo_grati = fields.Selection(selection=[("FP", "FIESTAS PATRIAS"), ("NV", "NAVIDAD"),("TR", "TRUNCA")])

    empleados_ids = fields.Many2many('hr.employee', string='Empleados' ,required=True)

    cts_ids = fields.One2many('hr_planilla.cts', 'cts_run_id', string='linea CTS')


    def compute_cts(self):

        if not self.empleados_ids:
            raise UserError("Seleccione al menos un empleado")

        self.env["hr_planilla.cts"].search([("cts_run_id.name", "=", self.name)]).unlink()

        periodo = str(self.period_id.code)
        anio = int(periodo[3:7])
        mes = int(periodo[0:2])

        obj_cts = self.env['hr_planilla.cts']
        obj_gratificaciones = self.env['hr_planilla.gratificaciones']


        for li in self.browse(self.ids):
            for emp in self.empleados_ids:

                gratificacion_ult = obj_gratificaciones.search(
                    [('empleado_id', '=', emp.id),
                     ('period_id', '=', self.period_grati_id.id),
                     ('tipo','=' , self.tipo_grati)
                     ])


                cts_dict = {
                    'cts_run_id' : li.id,
                    'empleado_id': emp.id,
                    'period_id' : self.period_id.id,
                    'anio': anio,
                    'tipo': self.tipo,
                    'ingresos': float(emp.contract_id.wage) + float(emp.asig_fam),
                    'ultima_gratificacion_neta' : gratificacion_ult.gratificacion_neta
                }


                cts = obj_cts.create(cts_dict)

                cts.calcular_cts()



