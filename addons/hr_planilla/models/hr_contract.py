from odoo import api, fields, models

class Contract(models.Model):
    _inherit = 'hr.contract'

    identification_id = fields.Char('Identificacion')
    es_renta_5ta = fields.Boolean('5ta categoria')

    @api.onchange('identification_id')
    def compute_employee(self):
        self.employee_id = self.env['hr.employee'].search([('identification_id', "=", self.identification_id)])

    def set_fecha_cese(self,date_end):
        self.date_end = date_end