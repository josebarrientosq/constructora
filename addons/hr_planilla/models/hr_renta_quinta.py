from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
import os

class renta_quinta(models.Model):
    _name = 'hr_planilla.renta_quinta'
    _description = 'Renta de 5ta categoria'


    empleado_id = fields.Many2one('hr.employee')

    period_id = fields.Many2one('account.period')
    periodo_mes = fields.Integer('mes',readonly=True)
    periodo_anio = fields.Integer('año',readonly=True)

    remuneracion_computable = fields.Float('renumeracion computable')
    resto_meses = fields.Integer('resto de meses', readonly=True)
    conceptos_anteriores = fields.Float('conceptos anteriores', readonly=True)
    rem_proyectada = fields.Float('remuneracion proyectada', readonly=True)
    gratificaciones = fields.Float('gratificaciones')
    ingreso_anual_proy = fields.Float('ingreso anual proyectado', readonly=True)

    deduccion = fields.Float('Deduccion 7 UIT', readonly=True)
    renta_neta_anual_proy = fields.Float('Renta neta anual proyectada', readonly=True)
    total_impuesto_proy = fields.Float('Total impuesto proyectado', readonly=True)
    retenciones_anteriores = fields.Float('Retenciones anteriores', readonly=True)
    retencion_mensual = fields.Float('Retencion mensual', readonly=True)



    def get_periodo_anio_mes(self):
        if self.period_id:
            periodo = str(self.period_id.code)
            self.periodo_anio = int(periodo[3:7])
            self.periodo_mes = int(periodo[0:2])

    def get_resto_meses(self):
        if self.period_id:
            self.resto_meses = 12 - self.periodo_mes + 1

    def get_conceptos_anteriores(self):
        self.conceptos_anteriores=0
        rentas_anteriores = self.env['hr_planilla.renta_quinta'].search([('empleado_id', '=', self.empleado_id.id),
                 ('periodo_anio', '=', self.periodo_anio),
                 ('periodo_mes', '<', self.periodo_mes)])
        if rentas_anteriores:
            for renta_anterior in rentas_anteriores:
                self.conceptos_anteriores = self.conceptos_anteriores + renta_anterior.remuneracion_computable

    def get_rem_proyectada(self):
        self.rem_proyectada = self.remuneracion_computable * self.resto_meses

    def get_gratificaciones(self):
        self.gratificaciones = self.remuneracion_computable*2

    def get_deduccion(self):
        uit = self.get_uit()
        if self.ingreso_anual_proy > (7*uit):
            self.deduccion = 7*uit
        else:
            self.deduccion = self.ingreso_anual_proy

    def get_renta_anual_proy(self):
        self.get_periodo_anio_mes()
        self.get_conceptos_anteriores()
        self.get_resto_meses()
        self.get_rem_proyectada()
        self.get_gratificaciones()
        self.ingreso_anual_proy = self.rem_proyectada + self.gratificaciones + self.conceptos_anteriores
        self.get_deduccion()
        self.renta_neta_anual_proy = self.ingreso_anual_proy - self.deduccion

    def get_retenciones_anteriores(self):
        self.retenciones_anteriores=0
        rentas_anteriores = ''

        if self.periodo_mes == 4:
            rentas_anteriores = self.env['hr_planilla.renta_quinta'].search([('empleado_id', '=', self.empleado_id.id),
                                                                         ('periodo_anio', '=', self.periodo_anio),
                                                                         ('periodo_mes', '<', 4)])

        if self.periodo_mes == 5 or self.periodo_mes == 6 or self.periodo_mes == 7 :
            rentas_anteriores = self.env['hr_planilla.renta_quinta'].search([('empleado_id', '=', self.empleado_id.id),
                                                                         ('periodo_anio', '=', self.periodo_anio),
                                                                         ('periodo_mes', '<', 5)])

        if self.periodo_mes == 8:
            rentas_anteriores = self.env['hr_planilla.renta_quinta'].search([('empleado_id', '=', self.empleado_id.id),
                                                                         ('periodo_anio', '=', self.periodo_anio),
                                                                         ('periodo_mes', '<', 8)])

        if self.periodo_mes == 9 or self.periodo_mes == 10 or self.periodo_mes == 11 :
            rentas_anteriores = self.env['hr_planilla.renta_quinta'].search([('empleado_id', '=', self.empleado_id.id),
                                                                         ('periodo_anio', '=', self.periodo_anio),
                                                                         ('periodo_mes', '<', 9)])
        if self.periodo_mes == 12:
            rentas_anteriores = self.env['hr_planilla.renta_quinta'].search([('empleado_id', '=', self.empleado_id.id),
                                                                         ('periodo_anio', '=', self.periodo_anio),
                                                                         ('periodo_mes', '<', 12)])

        if rentas_anteriores :
            for renta_anterior in rentas_anteriores:
                self.retenciones_anteriores = self.retenciones_anteriores + renta_anterior.retencion_mensual


    def get_uit(self):
        parametro_uit = self.env['hr_planilla.parametro_uit'].search([('fiscalyear_id', '=', self.period_id.fiscalyear_id.id)])
        if parametro_uit:
            return  parametro_uit.uit
        else:
            raise UserError("falta UIT")

    def get_parametro_renta(self):
        nivel=[0,0,0,0,0,0]
        porcentaje = [0, 0, 0, 0, 0, 0]

        uit = self.get_uit()
        parametros_renta = self.env['hr_planilla.parametro_renta_quinta'].search([])
        for parametro in parametros_renta:
            nivel[parametro.nivel] = parametro.limite_superior * uit
            porcentaje[parametro.nivel] = parametro.tasa/100

        return nivel,porcentaje

    def calcular_impuesto_renta(self):
        nivel , porcentaje = self.get_parametro_renta()
        renta=[0,0,0,0,0,0]

        i=1
        self.total_impuesto_proy=0
        renta_diferencia = self.renta_neta_anual_proy

        while renta_diferencia > 0:
            if renta_diferencia > nivel[i]-nivel[i-1]:
                renta[i] = nivel[i]-nivel[i-1]
            else:
                renta[i] = renta_diferencia

            renta_diferencia = renta_diferencia - renta[i]
            os.system("echo '%s'" % (renta_diferencia))

            self.total_impuesto_proy = self.total_impuesto_proy + renta[i]*porcentaje[i]

            i = i + 1


    def calcular_retencion_mensual(self):
        factor = [12, 12, 12, 9, 8, 8, 8, 5, 4, 4, 4, 1]
        self.retencion_mensual = (self.total_impuesto_proy - self.retenciones_anteriores)/factor[self.periodo_mes-1]

    def calcular_renta_todo(self):
        self.get_periodo_anio_mes()
        self.get_conceptos_anteriores()
        self.get_resto_meses()
        self.get_rem_proyectada()
        self.get_gratificaciones()
        self.ingreso_anual_proy = self.rem_proyectada + self.gratificaciones + self.conceptos_anteriores
        self.get_deduccion()
        self.renta_neta_anual_proy = self.ingreso_anual_proy - self.deduccion
        self.get_retenciones_anteriores()
        self.calcular_impuesto_renta()
        self.calcular_retencion_mensual()



        """
        renta1=0
        renta2=0
        renta3=0
        renta_diferencia = self.renta_neta_anual
        os.system("echo '%s'" % (renta_diferencia))
        if renta_diferencia > 5*4050:
            renta1 = 5*4050
        else :
            renta1 = renta_diferencia

        renta_diferencia = renta_diferencia - renta1
        os.system("echo '%s'" % (renta_diferencia))

        if renta_diferencia > 20 *4050 - 5*4050:
            renta2 = 20 *4050 - 5*4050
        else :
            renta2 = renta_diferencia

        renta_diferencia = renta_diferencia - renta2
        os.system("echo '%s'" % (renta_diferencia))

        self.total_impuesto_proy =  renta1*0.08 + renta2*0.14
        """
