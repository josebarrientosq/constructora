import time
from datetime import datetime
from datetime import time as datetime_time
import os

from odoo import api, fields, models, tools, _
from odoo.addons import decimal_precision as dp
from odoo.exceptions import UserError, ValidationError

class HrPayslip(models.Model):
    _inherit = 'hr.payslip'

    period_id = fields.Many2one('account.period', "Periodo" , required=True)
    identification = fields.Char('identificacion',related='employee_id.identification_id',readonly=True , required=True)
    basico = fields.Monetary('Básico',related='contract_id.wage')
    currency_id = fields.Many2one(string="Currency", related='company_id.currency_id', readonly=True)
#    asig_fam = fields.Float('A. Familiar' , related='employee_id.asig_fam')
    ingresos = fields.Float('Ingresos', readonly=True)
    descuentos = fields.Float('Descuentos', readonly=True)

    base_calculo = fields.Float('base calculo', readonly=True)

    aport_trab = fields.Float('Aporte Trab.', readonly=True)
    neto = fields.Float('Neto', readonly=True)
    aport_emp = fields.Float('Aporte Empl.', readonly=True)

    laborados = fields.Integer('Laborados')
    subsidiados = fields.Integer('Subsidiados')
    no_laborados_subsidiado = fields.Integer('No labo. y subsidiado')
    total_dias = fields.Integer('Total dias')

    horas_ordinaria = fields.Integer()
    minuto_ordinaria = fields.Integer()
    hora_sobretiempo = fields.Integer()
    minuto_sobretiempo = fields.Integer()
    total_horas = fields.Float()

    pago_haberes_transferencia = fields.Boolean('Pago haberes transferencia' , default = 'True')
    pago_cts_transferencia = fields.Boolean('Pago cts transferencia', default = 'True')


    def get_jornada_laboral(self):
        self.no_laborados_subsidiado = 0
        cont=0
        for jornada in self.worked_days_line_ids:
            if jornada.code=='SP' or jornada.code=='SI':
                cont = cont + jornada.number_of_days

        self.no_laborados_subsidiado = cont

        fecha_inicio = datetime.strptime(str(self.period_id.date_start), "%Y-%m-%d")
        fecha_fin = datetime.strptime(str(self.period_id.date_stop), "%Y-%m-%d")
        self.total_dias = int((fecha_fin - fecha_inicio).days)+1

        self.laborados = self.total_dias - self.no_laborados_subsidiado

    @api.multi
    def compute_sheet(self):
        for payslip in self:
            number = payslip.number or self.env['ir.sequence'].next_by_code('boleta_pago')
            # delete old payslip lines
            payslip.line_ids.unlink()
            # set the list of contract for which the rules have to be applied
            # if we don't give the contract, then the rules to apply should be for all current contracts of the employee
            contract_ids = payslip.contract_id.ids or \
                           self.get_contract(payslip.employee_id, payslip.date_from, payslip.date_to)
            lines = [(0, 0, line) for line in self._get_payslip_lines(contract_ids, payslip.id)]
            payslip.write({'line_ids': lines, 'number': number})
        return True

    @api.model
    def get_worked_day_lines(self, contracts, date_from, date_to):
        """
        @param contract: Browse record of contracts
        @return: returns a list of dict containing the input that should be applied for the given contract between date_from and date_to
        """
        res = []
        # fill only if the contract as a working schedule linked
        for contract in contracts.filtered(lambda contract: contract.resource_calendar_id):
            day_from = datetime.combine(fields.Date.from_string(date_from), datetime_time.min)
            day_to = datetime.combine(fields.Date.from_string(date_to), datetime_time.max)

            # compute leave days
            leaves = {}
            day_leave_intervals = contract.employee_id.iter_leaves(day_from, day_to, calendar=contract.resource_calendar_id)
            for day_intervals in day_leave_intervals:
                for interval in day_intervals:
                    holiday = interval[2]['leaves'].holiday_id
                    os.system("echo '%s'" % (holiday))
                    current_leave_struct = leaves.setdefault(holiday.holiday_status_id, {
                        'tipo': holiday.holiday_status_id.codigo,
                        'name': holiday.holiday_status_id.name or _('Global Leaves'),
                        'sequence': 5,
                        'code': holiday.holiday_status_id.suspension or 'GLOBAL',
                        'number_of_days': 0.0,
                        'number_of_hours': 0.0,
                        'contract_id': contract.id,
                    })
                    leave_time = (interval[1] - interval[0]).seconds / 3600
                    current_leave_struct['number_of_hours'] += leave_time
                    work_hours = contract.employee_id.get_day_work_hours_count(interval[0].date(), calendar=contract.resource_calendar_id)
                    if work_hours:
                        current_leave_struct['number_of_days'] += leave_time / work_hours

            # compute worked days
            work_data = contract.employee_id.with_context(no_tz_convert=True).get_work_days_data(day_from, day_to, calendar=contract.resource_calendar_id)
            attendances = {
                'name': "laborados",
                'sequence': 1,
                'code': 'LABORADO',
                'number_of_days': work_data['days'],
                'number_of_hours': work_data['hours'],
                'contract_id': contract.id,
            }


            res.append(attendances)
            res.extend(leaves.values())
        return res

    @api.multi
    def onchange_employee_id(self, date_from, date_to, employee_id=False, contract_id=False):
        # defaults
        res = {
            'value': {
                'line_ids': [],
                # delete old input lines
                'input_line_ids': [(2, x,) for x in self.input_line_ids.ids],
                # delete old worked days lines
                'worked_days_line_ids': [(2, x,) for x in self.worked_days_line_ids.ids],
                # 'details_by_salary_head':[], TODO put me back
                'name': '',
                'contract_id': False,
                'struct_id': False,
            }
        }
        if (not employee_id) or (not date_from) or (not date_to):
            return res
        ttyme = datetime.fromtimestamp(time.mktime(time.strptime(date_from, "%Y-%m-%d")))
        employee = self.env['hr.employee'].browse(employee_id)
        locale = self.env.context.get('lang') or 'en_US'
        res['value'].update({
            'name': 'nomina',
            'company_id': employee.company_id.id,
        })

        if not self.env.context.get('contract'):
            # fill with the first contract of the employee
            contract_ids = self.get_contract(employee, date_from, date_to)
        else:
            if contract_id:
                # set the list of contract for which the input have to be filled
                contract_ids = [contract_id]
            else:
                # if we don't give the contract, then the input to fill should be for all current contracts of the employee
                contract_ids = self.get_contract(employee, date_from, date_to)

        if not contract_ids:
            return res
        contract = self.env['hr.contract'].browse(contract_ids[0])
        res['value'].update({
            'name': contract.name,
            'contract_id': contract.id
        })
        struct = contract.struct_id
        if not struct:
            return res
        res['value'].update({
            'struct_id': struct.id,
        })
        # computation of the salary input
        contracts = self.env['hr.contract'].browse(contract_ids)
        worked_days_line_ids = self.get_worked_day_lines(contracts, date_from, date_to)
        input_line_ids = self.get_inputs(contracts, date_from, date_to)
        res['value'].update({
            'worked_days_line_ids': worked_days_line_ids,
            'input_line_ids': input_line_ids,
        })
        return res


    def get_gratificaciones(self):

        obj_gratificaciones = self.env['hr_planilla.gratificaciones']

        gratificacion = obj_gratificaciones.search(
            [('empleado_id', '=', self.employee_id.id),
             ('period_id', '=', self.period_id.id),
             ])

        if gratificacion:
            for li in self.browse(self.ids):
                grati_dict = {
                    'payslip_id': li.id,
                    'name': 'GRATIFICACION',
                    'amount': gratificacion.gratificacion_neta,
                    'code': 'GRATI',
                    'contract_id': self.contract_id.id

                }

                bonif_dict = {
                    'payslip_id': li.id,
                    'name': 'BONIF. EXTRAORD. TEMPORAL',
                    'amount': gratificacion.bonificacion_extraordinaria,
                    'code': 'BONIF',
                    'contract_id': self.contract_id.id

                }
            os.system("echo '%s'" % ("dict"))
            input_obj = self.env['hr.payslip.input']
            for input_line in li.input_line_ids:
                if input_line.code == 'GRATI':
                    input_line.unlink()

            for input_line in li.input_line_ids:
                if input_line.code == 'BONIF':
                    input_line.unlink()

            input_obj.create(grati_dict)
            input_obj.create(bonif_dict)

        else:
            os.system("echo '%s'" % ("No hay gratificacion"))


    def get_cts(self):

        obj_cts = self.env['hr_planilla.cts']

        cts = obj_cts.search(
            [('empleado_id', '=', self.employee_id.id),
             ('period_id', '=', self.period_id.id),
             ])

        if cts:
            for li in self.browse(self.ids):
                cts_planilla_dict = {
                    'payslip_id': li.id,
                    'name': 'CTS',
                    'amount': cts.monto_recibir,
                    'code': 'CTS',
                    'contract_id': self.contract_id.id

                }

            input_obj = self.env['hr.payslip.input']
            for input_line in li.input_line_ids:
                if input_line.code == 'CTS':
                    input_line.unlink()

            input_obj.create(cts_planilla_dict)

    @api.one
    def compute_renta_quinta(self):
        if self.contract_id.es_renta_5ta:
            obj_renta_quinta = self.env['hr_planilla.renta_quinta']

            renta_quinta_repetido = obj_renta_quinta.search(
                [('empleado_id', '=', self.employee_id.id),
                 ('period_id', '=', self.period_id.id)
                 ])
            if renta_quinta_repetido:
                renta_quinta_repetido.unlink()

            renta_quinta_dict = {
                'empleado_id': self.employee_id.id,
                'period_id': self.period_id.id,
                'remuneracion_computable': self.ingresos-self.descuentos,
            }

            renta_quinta=obj_renta_quinta.create(renta_quinta_dict)
            renta_quinta.calcular_renta_todo()

            for li in self.browse(self.ids):
                renta_dict = {
                    'payslip_id': li.id,
                    'name': 'RENTA de 5ta',
                    'amount': renta_quinta.retencion_mensual,
                    'code': 'RENTA5TA',
                    'contract_id': self.contract_id.id

                }

            input_renta = self.env['hr.payslip.input']
            for input_line in li.input_line_ids:
                if input_line.code == 'RENTA5TA':
                    input_line.unlink()

            input_renta.create(renta_dict)




    @api.one
    def mostrar_totales_categoria(self):
        ingresos = 0
        descuentos = 0
        ingresos_afectos = 0
        descuentos_afectos = 0
        aport_trab = 0
        aport_emp = 0
        for det in self.line_ids:
            if det.category_id.code == 'Ingresos':
                ingresos += det.total
                if not det.salary_rule_id.inafecto_aportacion:
                    ingresos_afectos += det.total

            if det.category_id.code == 'Descuentos':
                descuentos += det.total
                if not det.salary_rule_id.inafecto_aportacion:
                    descuentos_afectos += det.total

            if det.category_id.code == 'Apor_trab':
                aport_trab += det.total

            if det.category_id.code == 'Apor_emp':
                aport_emp += det.total

        self.ingresos = ingresos
        self.descuentos = descuentos
        self.base_calculo = ingresos_afectos - descuentos_afectos
        self.aport_trab = aport_trab
        self.neto = ingresos - descuentos - aport_trab
        self.aport_emp = aport_emp

    @api.one
    def ejecutar_calculos(self):

        self.compute_sheet() #generar los conceptos
        self.mostrar_totales_categoria() # para actualizar los totales de ingresos , descuentos y base de calculo
        if self.contract_id.es_renta_5ta:
            self.compute_renta_quinta()

        self.get_gratificaciones()
        self.get_cts()


        self.compute_sheet() #generar los aportes basados en el calculo anterior

        self.get_jornada_laboral()
        self.mostrar_totales_categoria() # para actualizar los totales de los aportes con la base de calculo



    def set_period(self,periodo):
        self.period_id = periodo

"""
    @api.onchange('worked_days_line_ids')
    def actualizar_cambio(self):
        self.compute_sheet()
"""

class HrPayslipWorkedDays(models.Model):
    _inherit = 'hr.payslip.worked_days'

    tipo = fields.Char('Tipo')
    number_of_min = fields.Float('minutos')
