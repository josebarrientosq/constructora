from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
import os

class pagos_planilla(models.Model):
    _name = 'hr_planilla.pagos'
    _description = 'Pagos planilla'

    name = fields.Char(string='Pago')
    period_id = fields.Many2one('account.period', 'Periodo')

    tipo = fields.Selection(selection=[
        ('HABERES', 'HABERES'),
        ('CTS', 'CTS'),
    ], default='HABERES')

    state = fields.Selection(selection=[
        ('draft', 'Borrador'),
        ('generated', 'Generado'),
        ('exported', 'Exportado'),
    ], default='draft')

    linea = fields.One2many('hr_planilla.pagos_linea', 'pago_id', string="detalle de pago")

    @api.one
    def recargar_haberes(self):
        linea_obj = self.env['hr_planilla.pagos_linea']
        docs = self.env["hr.payslip"].search([("period_id","=",self.period_id.code),("pago_haberes_transferencia","=","True")])
        for li in self.browse(self.ids):
            for doc in docs:
                linea_obj.create({
                    'base_id': li.id,
                    'period_id' : doc.period_id.id,
                    'employee_id': doc.employee_id.id,
                    'ingresos' : doc.ingresos,
                    'descuentos' : doc.descuentos,
                    'base_calculo' : doc.base_calculo,

                    'aport_trab' : doc.aport_trab,

                })
        return True


class pagos_linea(models.AbstractModel):
    _name = 'hr_planilla.pagos_linea'

    pago_id = fields.Many2one('hr_planilla.pagos')
    period_id = fields.Many2one('account.period', 'Periodo')
    employee_id = fields.Many2one('hr.employee','Empleado')

    ingresos = fields.Float('Ingresos', readonly=True)
    descuentos = fields.Float('Descuentos', readonly=True)

    base_calculo = fields.Float('base calculo', readonly=True)

    aport_trab = fields.Float('Aporte Trab.', readonly=True)
    neto = fields.Float('Neto', readonly=True)
    devolucion = fields.Float('Devolucion')
    a_pagar = fields.Float('A pagar', readonly=True )


    @api.onchange('devolucion')
    def calcular_pago(self):
        self.a_pagar = self.neto - self.devolucion