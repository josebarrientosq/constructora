from odoo import models, fields, api

class HrSalaryRule(models.Model):
    _inherit = 'hr.salary.rule'

    inafecto_aportacion = fields.Boolean('Inafecto aportación' , default=False)