# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Rmv (models.Model):
    _name = "hr_rmv"
    _description = "Remuneración mínima vital"

    rmv = fields.Float('RMV')