from odoo import fields,models,api 

class AccountInvoice(models.Model):
    _inherit = ['account.invoice']

    numero_documento = fields.Char(string = "Número de documento",
                                    related="partner_id.vat")
    estado_contribuyente = fields.Selection(selection=[('activo', 'Activo'), ('noactivo', 'No Activo')], 
                                            string='Estado del Contribuyente',
                                            related="partner_id.estado_contribuyente")

    tipo_documento = fields.Many2one('einvoice.catalog.06', 'Tipo Doc.', index=True,related="partner_id.catalog_06_id")                                    

    direccion_completa = fields.Char(string="Dirección",related="partner_id.street")

    estado_habido = fields.Selection(selection=[('habido', 'Habido'), ('nhabido', 'No Habido')], string='Estado',related="partner_id.state")