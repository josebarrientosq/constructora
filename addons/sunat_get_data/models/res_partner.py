# -*- coding: utf-8 -*-
from openerp import models, fields, api
from odoo.exceptions import UserError, ValidationError
import requests
from PIL import Image
import json
import pytesseract
from io import StringIO,BytesIO
from bs4 import BeautifulSoup
import os

def esrucvalido(dato):
    largo_dato = len(dato)
    if dato is not None and dato != "" and dato.isdigit() and (largo_dato == 11 or largo_dato == 8):
        valor = int(dato)
        if largo_dato == 8:
            suma = 0
            for i in range(largo_dato-1):
                digito = int(dato[i]) - 0
                if i == 0:
                    suma = suma + digito * 2
                else:
                    suma = suma + digito * (largo_dato - 1)
                resto = suma % 11
                if resto == 1:
                    resto = 11
                if (resto + int(dato[largo_dato-1]) - 0) == 11:
                    return True

        elif largo_dato == 11:
            suma = 0
            x = 6
            for i in range(largo_dato - 1):
                if i == 4:
                    x = 8
                digito = int(dato[i]) - 0
                x = x - 1
                if x == 0:
                    suma = suma + digito * x
                else:
                    suma = suma + digito * x
            resto = suma % 11
            resto = 11 -resto
            if resto >= 10:
                resto = resto - 10
            if resto == int(dato[largo_dato - 1]) - 0:
                return True

        return False
    else:
        return False


def esdocvalido(dato, tipo):
    largo_dato = len(dato)
    if dato is not None and dato != "":
        if tipo == '1':
            if dato.isdigit() and largo_dato == 8:
                return True
        else:
            return True
    else:
        return False

def get_data_doc(tipoConsulta, search1, tipoDoc=""):
    flag_error = False
    data_error = []
    data_enviar = {}
    # VALIDACION
    if tipoConsulta == 1:
        if not esrucvalido(search1):
            flag_error = True
            data_error.append("Ruc no válido")
    elif tipoConsulta == 2:
        if len(search1) < 4 or tipoDoc not in ['1', '4', '7', 'A']:
            flag_error = True
            data_error.append("Documento no válido")
        elif not esdocvalido(search1, tipoDoc):
            flag_error = True
            data_error.append("Documento no válido")
    elif tipoConsulta == 3:
        if len(search1) < 4:
            flag_error = True
            data_error.append("Razón Social no válida")
    else:
        flag_error = True
        data_error.append("Método no válido")

    if flag_error:
        return {
            'codeResponse': 400,
            'error': data_error
        }

    s = requests.Session()
    r = s.get('http://www.sunat.gob.pe/cl-ti-itmrconsruc/captcha?accion=image')
    """
    f = open('/mnt/extra-addons/sunat_get_data/controllers/captcha.jpg', 'wb')
    with f:
        f.write(r.content)
        f.close()
    """
    img = Image.open(BytesIO(r.content))
    captcha_val = pytesseract.image_to_string(img)

    # Tipo de Busqueda 1-RUC, 2-Tipo Documento, 3

    if tipoConsulta == 1:
        data_enviar["accion"] = 'consPorRuc'
        data_enviar["nroRuc"] = search1
        data_enviar["search1"] = search1
        data_enviar["tipdoc"] = ''
        data_enviar["search2"] = ''
        data_enviar["search3"] = ''
    elif tipoConsulta == 2:
        data_enviar["accion"] = 'consPorTipdoc'
        data_enviar["nroRuc"] = ''
        data_enviar["nrodoc"] = search1
        data_enviar["tipdoc"] = tipoDoc
        data_enviar["search1"] = ''
        data_enviar["search2"] = search1
        data_enviar["search3"] = ''
    else:
        data_enviar["accion"] = 'consPorRazonSoc'
        data_enviar["nroRuc"] = ''
        data_enviar["nrodoc"] = ''
        data_enviar["tipdoc"] = ''
        data_enviar["razSoc"] = search1
        data_enviar["search1"] = ''
        data_enviar["search2"] = ''
        data_enviar["search3"] = search1

    data_enviar["rbtnTipo"] = tipoConsulta
    data_enviar["contexto"] = "ti-it"
    data_enviar["modo"] = "1"
    data_enviar["codigo"] = captcha_val

    consult = None
    try:
        consult = s.post('http://www.sunat.gob.pe/cl-ti-itmrconsruc/jcrS00Alias', data=data_enviar,timeout=5)
        os.system("echo '%s'"%(str(consult)))
    except:
        raise UserError("No se pudo obtener los datos, vuelva a intentarlo luego.")
    
    """
    f = open('/mnt/extra-addons/sunat_get_data/controllers/response.html', 'w')
    with f:
        f.write(consult.text.encode('utf-8'))
        f.close()
    """
    
    datos = {}
    if consult:
        soup = BeautifulSoup(consult.text, 'html.parser')
    else:
        raise UserError("No se pudo obtener los datos, vuelva a intentarlo luego.")

    if tipoDoc=="1":
        table = soup.find("table",{"cellspacing":1})
        if table:
            d = table.find_all("tr")
            if d:
                d=d[1]
                os.system("echo '%s'"%(str(d)))
                datos["nombres"] = d.find_all("td")[1].get_text()
                datos["nombreComercial"] = d.find_all("td")[1].get_text()
                datos["provincia"] = d.find_all("td")[2].get_text()
                datos["estado"] = d.find_all("td")[3].get_text()

                os.system("echo '%s'"%(str(datos)))
                return {
                    'codeResponse': consult.status_code,
                    'data': datos
                }
            else:
                return {
                    "error":"Vuelva a intentarlo luego"
                }
        else:
            return {
                    "error":"Vuelva a intentarlo luego"
                }
    else:
        table = soup.find("table")
    
    if table:
        rows = table.find_all("tr")
        
        rucNombre = rows[0].find_all("td")[1].get_text()
        datos["ruc"] = rucNombre[0:11]
        datos["nombre"] = rucNombre[14:]
        datos["tipoContribuyente"] = rows[1].find_all("td")[1].get_text()
        i = 0
        if datos["ruc"][0:2] == '10':
            i = 0
            datos["tipoDoc"] = "06"
            datos["tipoPersona"] = "PN"
            datos["tipoDocumento"] = rows[2 + i].find_all("td")[1].get_text().replace("\n", "").replace("\t",
                                                                                                        "").replace(
                "\r", "").replace("   ", "")
            datos["profesion"] = rows[6 + i].find_all("td")[3].get_text().strip()
        elif datos["ruc"][0:2] == '20':
            i = -1
            datos["tipoDoc"] = "01"
            datos["tipoPersona"] = "PJ"
        datos["nombreComercial"] = rows[3 + i].find_all("td")[1].get_text()
        datos["fechaInscripcion"] = rows[4 + i].find_all("td")[1].get_text()
        datos["fechaIniActividades"] = rows[4 + i].find_all("td")[3].get_text().strip()
        datos["estado"] = rows[5 + i].find_all("td")[1].get_text().strip()
        datos["condicionContribuyente"] = rows[6 + i].find_all("td")[1].get_text().strip()
        datos["domicilioFiscal"] = rows[7 + i].find_all("td")[1].get_text().strip()
        dom_split = datos["domicilioFiscal"].strip().split("-")
        if len(dom_split) == 3:
            datos["domicilioFiscal"] = dom_split[0]
            datos["provincia"] = dom_split[1].strip().replace("\n", "")
            datos["distrito"] = dom_split[2].strip().replace("\n", "")
        else:
            datos["provincia"] = ""
            datos["distrito"] = ""
        datos["sistemaEmision"] = rows[8 + i].find_all("td")[1].get_text().strip()
        datos["actividadComercioExterior"] = rows[8 + i].find_all("td")[3].get_text().strip()
        datos["sistemaContabilidad"] = rows[9 + i].find_all("td")[1].get_text().strip()
        act_econ = []
        for opt in rows[10 + i].find_all("td")[1].find_all("option"):
            act_econ.append(opt.get_text())
        datos["actEconomicas"] = act_econ
        comprobantes = []
        for opt in rows[11 + i].find_all("td")[1].find_all("option"):
            comprobantes.append(opt.get_text())
        datos["comprobantesElec"] = comprobantes
        sistemEmision = []
        for opt in rows[12 + i].find_all("td")[1].find_all("option"):
            sistemEmision.append(opt.get_text())
        datos["sistemasEmision"] = sistemEmision
        datos["fechaInicioEmisorE"] = rows[13 + i].find_all("td")[1].get_text().strip()
        datos["comElectronica"] = rows[14 + i].find_all("td")[1].get_text().strip()
        datos["fechaPLE"] = rows[15 + i].find_all("td")[1].get_text().strip()
        padrones = []
        for opt in rows[16 + i].find_all("td")[1].find_all("option"):
            padrones.append(opt.get_text())
        datos["padrones"] = padrones
        return {
            'codeResponse': consult.status_code,
            'data': datos
        }
    else:
        return {
            'codeResponse': consult.status_code,
            'error': 'No se pudo conectar al servidor'
        }


def get_data_doc_number(tipoConsulta, numero_doc, format='json'):
    res={}
    try:
        response = get_data_doc(tipoConsulta, numero_doc)
    except requests.exceptions.ConnectionError as e:
        res['message'] = 'Error en la conexion'
        return res

    if response["codeResponse"] == 200:
        res['error'] = False
        res['data'] = response["data"]
    else:
        try:
            res['message'] = response["error"]
        except Exception as e:
            res['error'] = True
    return res



class ResPartner(models.Model):
    _inherit = 'res.partner'

    registration_name = fields.Char('Name', size=128, index=True)
    catalog_06_id = fields.Many2one('einvoice.catalog.06', 'Tipo Doc.', index=True)
    state = fields.Selection([('habido', 'Habido'), ('nhabido', 'No Habido')], 'Estado')
    estado_contribuyente = fields.Selection([('activo', 'Activo'), ('noactivo', 'No Activo')], 'Estado del Contribuyente')

    def _esrucvalido(self, dato):
        largo_dato = len(dato)
        if dato is not None and dato != "" and dato.isdigit() and (largo_dato == 11 or largo_dato == 8):
            valor = int(dato)
            if largo_dato == 8:
                suma = 0
                for i in range(largo_dato - 1):
                    digito = int(dato[i]) - 0
                    if i == 0:
                        suma = suma + digito * 2
                    else:
                        suma = suma + digito * (largo_dato - 1)
                    resto = suma % 11
                    if resto == 1:
                        resto = 11
                    if (resto + int(dato[largo_dato - 1]) - 0) == 11:
                        return True
            elif largo_dato == 11:
                suma = 0
                x = 6
                for i in range(largo_dato - 1):
                    if i == 4:
                        x = 8
                    digito = int(dato[i]) - 0
                    x = x - 1
                    if x == 0:
                        suma = suma + digito * x
                    else:
                        suma = suma + digito * x
                resto = suma % 11
                resto = 11 - resto
                if resto >= 10:
                    resto = resto - 10
                if resto == int(dato[largo_dato - 1]) - 0:
                    return True

            return False
        else:
            return False

    @api.onchange('catalog_06_id', 'vat')
    def vat_change(self):
        self.update_document()

    def consulta_ruc_api(self):
        api_ruc_username = self.company_id.api_ruc_username
        api_ruc_password = self.company_id.api_ruc_password
        api_ruc_endpoint = self.company_id.api_ruc_endpoint
        errors = []
        if not api_ruc_endpoint:
            errors.append("Debe configurar el end-point del API RUC")
        if not api_ruc_username:
            errors.append("Debe configurar el username del API RUC")
        if not api_ruc_password:
            errors.append("Debe configurar el password del API RUC")

        if len(errors)==0:
            data = {"username":api_ruc_username,"password":api_ruc_password}
            header = {"content-type":"application/json"}
            url = api_ruc_endpoint+"/login"
            response = requests.request("POST",url=url,data=json.dumps(data),headers=header)
            response = json.loads(response.text)
            token = response.get("access_token",False)
            if token:
                header["authorization"] = "Bearer "+token
                url = api_ruc_endpoint + "/empresa"
                data = {"ruc":self.vat}
                response = requests.request("POST",url=url,data=json.dumps(data),headers=header)
                response = json.loads(response.text)
                return response
        else: 
            raise UserError("\n".join(errors))
        return False


    @api.one
    def update_document(self):
        if not self.vat:
            return False
        if self.catalog_06_id and self.catalog_06_id.code == '1':
            # Valida DNI
            if self.vat and len(self.vat) != 8:
                raise UserError('El Dni debe tener 8 caracteres')
            else:
                d = get_data_doc(2, self.vat, '1')
                if "error" not in d:
                    d = d['data']
                    self.name = d['nombres']
                    self.street = "-"
                    self.zip = "-"
                    self.registration_name = d['nombres']
                    self.estado_contribuyente = "activo"
                    self.state = 'habido'
                    prov_ids = self.env['res.country.state'].search([('name', '=', d['provincia'] )])
                    if prov_ids.exists():
                        self.province_id = prov_ids[0]
                        
        elif self.catalog_06_id and self.catalog_06_id.code == '6':
            # Valida RUC
            if self.vat and len(self.vat) != 11:
                raise UserError('El Ruc debe tener 11 caracteres')
            if not self._esrucvalido(self.vat):
                raise UserError('El Ruc no es valido')
            else:
                d = self.consulta_ruc_api()
                #d = get_data_doc(1, self.vat)
                if not d:
                    raise UserError("Error")
                if not d["success"]:
                    raise UserError("Error")
                #d = d['data']
                # ~ Busca el distrito
                ditrict_obj = self.env['res.country.state']
                prov_ids = ditrict_obj.search([('name', '=', d['provincia']),
                                               ('province_id', '=', False),
                                               ('state_id', '!=', False)])
                dist_id = ditrict_obj.search([('name', '=', d['distrito']),
                                              ('province_id', '!=', False),
                                              ('state_id', '!=', False),
                                              ('province_id', 'in', [x.id for x in prov_ids])], limit=1)
                if dist_id:
                    self.district_id = dist_id.id
                    self.province_id = dist_id.province_id.id
                    self.state_id = dist_id.state_id.id
                    self.country_id = dist_id.country_id.id

                # Si es HABIDO, caso contrario es NO HABIDO
                tstate = d['condicion_de_domicilio']
                if tstate == 'HABIDO':
                    tstate = 'habido'
                else:
                    tstate = 'nhabido'
                tstate_contribuyente = d['estado_del_contribuyente']

                if tstate_contribuyente=="ACTIVO":
                    self.estado_contribuyente = "activo"
                else:
                    self.estado_contribuyente = "noactivo"

                self.state = tstate
                self.name = d['nombre']
                self.registration_name = d['nombre']
                self.zip = d["ubigeo"]
                self.street = d['direccion_completa']
                self.vat_subjected = True
                self.is_company = True
        else:
            True