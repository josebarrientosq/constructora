from odoo import fields,models,api
from odoo import exceptions, _
from odoo.exceptions import ValidationError, UserError

import logging
_logger = logging.getLogger(__name__)

class Orderpoint(models.Model):
    """ Defines Minimum stock rules. """
    _inherit= "stock.warehouse.orderpoint"

    product_qty_available = fields.Float(
        compute="_get_product_stock",
        string="A mano"
    )

    state = fields.Selection([('stock_disponible', 'stock disponible'), ('stock_deficiente', 'stock deficiente')],readonly =True ,string = "Estado")

    @api.multi
    def _get_product_stock(self):
        for rec in self:
            rec._get_product_stock_product()

    @api.multi
    def _get_product_stock_product(self):
        _logger.debug("location: %s", self.location_id.id)
        ctx = dict(self._context, location=self.location_id.id)

        product = self.env['product.product'].with_context(ctx).browse(self.product_id.id)
        _logger.debug("qty_available: %s", product.qty_available)
        _logger.debug("product: %s", product)
        self.product_qty_available = product.qty_available

        dif = self.product_qty_available - self.product_min_qty
        if dif > 0:
            self.state = "stock_disponible"
        else:

            self.state = "stock_deficiente"
