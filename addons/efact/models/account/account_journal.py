# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo.exceptions import UserError

class AccountJournal(models.Model):
    _inherit = "account.journal"
    codigo_documento = fields.Char("Codigo de tipo de Documento")
    tipo_envio = fields.Selection(selection=[("0","0 - Pruebas"),("1","1 - Homologación"),("2","2 - Producción")])

    formato_comprobante = fields.Selection(selection=[("fisico","Físico"),("electronico","Electrónico")],default="electronico")

    def _list_invoice_type(self):
        catalogs=self.env["einvoice.catalog.01"].search([])
        list=[]
        for cat in catalogs:
            list.append((cat.code,cat.name))
        return list

    invoice_type_code_id=fields.Selection(
        string="Tipo de Documento",
        selection=_list_invoice_type)
    
    @api.model
    def _get_sequence_prefix(self, code, refund=False):
        prefix = code.upper()
        if refund and (self.formato_comprobante == 'electronico'):
            prefix=prefix[0]+'R'+prefix[2:]
        elif refund:
            prefix="R"+prefix
        return prefix + '-'

    @api.model
    def create(self, vals):
        if ("invoice_type_code_id" in vals) and (vals.get("formato_comprobante","") == 'electronico'):
            if vals["invoice_type_code_id"] in ['01','03']:
                if "code" in vals:
                    if len(vals["code"])!=4:
                        raise UserError("La serie debe contener 4 carácteres. Si es Factura inicia con 'F' y si es boleta inicia con 'B'")
                    if vals["invoice_type_code_id"]=='01':
                        if vals["code"][0] != 'F':
                            raise UserError("La serie de una factura debe iniciar con 'F'")
                    elif vals["invoice_type_code_id"]=='03':                    
                        if vals["code"][0] != 'B':
                            raise UserError("La serie de una factura debe iniciar con 'B'")

        return super(AccountJournal,self).create(vals)
    
    @api.multi
    def write(self, vals):
        if ("invoice_type_code_id" in vals) and (vals.get("formato_comprobante","") == 'electronico'):
            if vals["invoice_type_code_id"] in ['01','03','07','08']:
                if "code" in vals:
                    if len(vals["code"])!=4:
                        raise UserError("La serie debe contener 4 carácteres. Si es Factura inicia con 'F' y si es boleta inicia con 'B'")

                    if vals["invoice_type_code_id"]=='01':
                        if vals["code"][0] != 'F':
                            raise UserError("La serie de una factura debe iniciar con 'F'")
                            
                    elif vals["invoice_type_code_id"]=='03':                    
                        if vals["code"][0] != 'B':
                            raise UserError("La serie de una factura debe iniciar con 'B'")

        return super(AccountJournal,self).write(vals)
    
    @api.constrains("code")
    def _constrains_serie(self):
        if self.formato_comprobante == 'electronico':
            if len(self.code)!=4:
                raise UserError("La serie debe contener 4 carácteres. Si es Factura inicia con 'F' y si es boleta inicia con 'B'")

            if self.invoice_type_code_id=='01':
                if self.code[0] != 'F':
                    raise UserError("La serie de una factura debe iniciar con 'F'")
                    
            elif self.invoice_type_code_id=='03':                    
                if self.code[0] != 'B':
                    raise UserError("La serie de una factura debe iniciar con 'B'")

    @api.model
    def _create_sequence(self, vals, refund=False):
        """ Create new no_gap entry sequence for every new Journal"""
        prefix = self._get_sequence_prefix(vals['code'], refund)
        seq = {
            'name': refund and vals['name'] + _(': Refund') or vals['name'],
            'implementation': 'no_gap',
            'prefix': prefix,
            'padding': 8,
            'number_increment': 1,
            'use_date_range': False,
            'invoice_type_code_id':'08'
        }
        if 'company_id' in vals:
            seq['company_id'] = vals['company_id']
        return self.env['ir.sequence'].create(seq)
