#!/usr/bin/env python2

from odoo import models,api,fields 
from odoo.exceptions import UserError
from datetime import datetime
import os
import json
from ..auth.oauth import enviar_doc_resumen_url,generate_token_by_company,extaer_estado_emision,extraer_error
import requests

class AccountInvoice(models.Model):
    _inherit = "account.invoice"
    account_summary_id = fields.Many2one("account.summary",
                                        string="Resumen Diario",
                                        ondelete="set default",
                                        default=False)

#https://1bkdtrxjjh.execute-api.us-east-1.amazonaws.com/Prod/api

class AccountSummaryLine(models.Model):
    _name = "account.summary.line"   
    account_summary_id = fields.Many2one("account.summary",string="Resumen Diario",ondelete="cascade")
    serie = fields.Char("Serie")
    tipo_documento = fields.Selection(string="Tipo de Documento",selection=[('03',"Boleta"),("07","Nota de Crédito"),("08","Nota de Débito")])
    tipo_moneda = fields.Char("Tipo de Moneda")
    numero_documento_inicio = fields.Char("Número de Documento Inicio")
    numero_documento_fin = fields.Char("Número de Documento Fin")
    monto_igv = fields.Float("Monto IGV",digits=(10,2))
    monto_isc = fields.Float("Monto ISC",digits=(10,2))
    monto_total = fields.Float("Monto Total",digits=(10,2))
    monto_neto = fields.Float("Monto Neto",digits=(10,2))
    monto_exe = fields.Float("Monto Inafecto",digits=(10,2))
    monto_exo = fields.Float("Monto Exonerado",digits=(10,2))
    monto_grat = fields.Float("Monto Gratuito",digits=(10,2))
    monto_exp = fields.Float("Monto Exportación",digits=(10,2))
    monto_otros = fields.Float("Monto Otros",digits=(10,2))
    

class AccountSummary(models.Model):
    _name = "account.summary"
    _rec_name = "identificador_resumen"

    company_id = fields.Many2one("res.company",required=True,string="Compañia")
    fecha_generacion = fields.Date("Fecha de Generación",default=datetime.now(),required="True")
    fecha_emision_documentos = fields.Date("Fecha de Emisión de Documentos",default=datetime.now(),required="True")
    identificador_resumen = fields.Char("Identificador de Resumen",default="Resumen Diario",)
    summary_line_ids = fields.One2many("account.summary.line","account_summary_id",string="Líneas de Resumen")
    resumen_diario_json = fields.Text("Resumen Diario JSON")
    account_invoice_ids = fields.One2many("account.invoice","account_summary_id",string="Comprobantes")
    ticket = fields.Char("Ticket")
    estado = fields.Selection(selection=[("borrador","Borrador"),
                                        ("enviado","Enviado"),
                                        ("resumen_valido","Resumen Valido"),
                                        ("resumen_rechazado","Resumen Rechazado")],default="borrador")
    estado_emision = fields.Selection([
            ('A', 'Aceptado'),
            ('E', 'Enviado a SUNAT'),
            ('N', 'Envio Erroneo'),
            ('O', 'Aceptado con Observacion'),
            ('R', 'Rechazado'),
            ('P', 'Pendiente de envio a SUNAT'),
        ], string="Estado Emision a SUNAT", readonly=True)

    json_respuesta = fields.Text(string="JSON de respuesta")

    

    def default_numero_envio(self):
        documets = self.env['account.summary'].search_read(
            [['estado', 'not in', ['borrador']], ['fecha_emision_documentos', '=', self.fecha_emision_documentos]],
            {'order':'DESC numero_envio'})
        if len(documets)==0:
            return 1
        else:
            return documets[0].numero_envio + 1
            
    numero_envio = fields.Integer(string="Número de Envío",default=default_numero_envio)

    @api.constrains
    def consistencia_fecha_generacion_emision(self):
        if self.fecha_emision_documentos>self.fecha_generacion:
            raise UserError("La fecha de generación del Resumen Diario debe ser mayor o igual a las fecha de emisión de los comprobantes")

    @api.onchange("fecha_generacion","fecha_emision_documentos")
    def generar_identificador_resumen(self):
        fecha = datetime.strptime(self.fecha_generacion, "%Y-%m-%d").strftime("%Y%m%d")
        self.identificador_resumen = "RC-"+fecha+"-"+str(self.numero_envio).zfill(3)

    def cargar_comprobantes(self): 
        account_invoices = self.env["account.invoice"].search([("date_invoice","=",self.fecha_emision_documentos),
                                                                ("state","in",["open","paid"]),
                                                                ("invoice_type_code","=","03")])
        account_invoices = [b for b in account_invoices]

        nota_credito_ids = self.env["account.invoice"].search([("date_invoice","=",self.fecha_emision_documentos),
                                                                    ("state","in",["open","paid"]),
                                                                    ("invoice_type_code","=","07")])


        nota_credito_ids = [nc for nc in nota_credito_ids if nc.refund_invoice_id.invoice_type_code == "03"]

        account_invoices = account_invoices + nota_credito_ids
        account_invoices = [{
                            "id":invoice.id,
                            "serie":invoice.number.split("-")[0],
                            "numero":int(invoice.number.split("-")[1]),
                            "tipo_documento":invoice.invoice_type_code,
                            "monto_total":invoice.amount_total,
                            "monto_neto":invoice.total_venta_gravado,
                            "monto_igv":invoice.amount_tax,
                            "monto_isc":0.0,
                            "monto_exe":invoice.total_venta_inafecto,
                            "monto_exo":invoice.total_venta_exonerada,
                            "monto_grat":invoice.total_venta_gratuito,
                            "monto_exp":0.0,
                            "monto_otros":0.0,
                            "tipo_moneda":invoice.currency_id.name
                        } for invoice in account_invoices]

        self.account_invoice_ids = [account_invoice["id"] for account_invoice in account_invoices]

        if self.summary_line_ids:
            for sl in self.summary_line_ids:
                sl.unlink()
        self.summary_line_ids = self.summary(account_invoices)


    def summary_lines(self,lines):
        return {
            "serie":lines["serie"],
            "tipo_documento":lines["tipo_documento"],
            "tipo_moneda":lines["tipo_moneda"],
            "numero_documento_inicio":min(lines["lines"], key=lambda x:x['numero'])["numero"],
            "numero_documento_fin":max(lines["lines"], key=lambda x:x['numero'])["numero"],
            "monto_igv":sum([line["monto_igv"] for line in lines["lines"]]),
            "monto_isc":sum([line["monto_isc"] for line in lines["lines"]]),
            "monto_total":sum([line["monto_total"] for line in lines["lines"]]),
            "monto_neto":sum([line["monto_neto"] for line in lines["lines"]]),
            "monto_exe":sum([line["monto_exe"] for line in lines["lines"]]),
            "monto_exo":sum([line["monto_exo"] for line in lines["lines"]]),
            "monto_exp":sum([line["monto_exp"] for line in lines["lines"]]),
            "monto_grat":sum([line["monto_grat"] for line in lines["lines"]]),
            "monto_otros":sum([line["monto_otros"] for line in lines["lines"]]),
            "account_summary_id":self.id
        }

    def summary(self,invoices):
        lines = {}
        for invoice in invoices:
            grupo = invoice["serie"]+"-"+invoice["tipo_documento"]+"-"+invoice["tipo_moneda"]
            if lines.get(grupo):
                lines[grupo]["lines"].append(invoice)
            else:
                lines[grupo] = {}
                lines[grupo]["serie"] = invoice["serie"]
                lines[grupo]["tipo_documento"] = invoice["tipo_documento"]
                lines[grupo]["tipo_moneda"] = invoice["tipo_moneda"]
                lines[grupo]["lines"] = [invoice]

        for line in lines:
            lines[line]["lines"] = sorted(lines[line]["lines"],key=lambda k:k['numero'])

        

        lines_2 = {}
        for line in lines:
            minNumero = min(lines[line]["lines"], key=lambda x:x['numero'])["numero"]
            maxNumero = max(lines[line]["lines"], key=lambda x:x['numero'])["numero"]
            line_cnt = 0
            cnt = 0
            for i in range(minNumero,maxNumero+1):
                if lines[line]["lines"][cnt]["numero"] == i:
                    line_x = line+"-"+str(line_cnt)
                    if lines_2.get(line_x):
                        lines_2[line_x]["lines"].append(lines[line]["lines"][cnt]) 
                    else:
                        lines_2[line_x] = {}
                        lines_2[line_x]["serie"] = lines[line]["serie"]
                        lines_2[line_x]["tipo_documento"] = lines[line]["tipo_documento"]
                        lines_2[line_x]["tipo_moneda"] = lines[line]["tipo_moneda"]
                        lines_2[line_x]["lines"] = [lines[line]["lines"][cnt]]
                    cnt +=1
                else:
                    line_cnt+=1
        
        return [self.summary_lines(lines_2[line]) for line in lines_2]


    @api.multi
    def generar_resumen_diario(self):
        for record in self:
            resumen_diario_json = {}
            resumen_diario_json["resumen"] = {
                "numDocEmisor":record.company_id.partner_id.vat,
                "tipoDocEmisor":record.company_id.partner_id.catalog_06_id.code,
                "fechaReferente":record.fecha_emision_documentos,
                "nombreEmisor":record.company_id.partner_id.name,
                "id":record.numero_envio
            }
            resumen_diario_json["fechaGeneracion"] = record.fecha_generacion
            resumen_diario_json["tipoResumen"] = "RC"
            resumen_diario_json["idTransaccion"] = record.identificador_resumen
            resumen_diario_json["detalle"]=[]
            for summary_line in record.summary_line_ids:
                resumen_diario_json["detalle"].append({
                    "serie":summary_line.serie,
                    "tipoDocumento":summary_line.tipo_documento,
                    "numDocumentoInicio":summary_line.numero_documento_inicio,
                    "numDocumentoFin":summary_line.numero_documento_fin,
                    "tipoMoneda":summary_line.tipo_moneda,
                    "mntIgv":summary_line.monto_igv,
                    "mntIsc":summary_line.monto_isc,
                    "mntTotal":summary_line.monto_total,
                    "mntNeto":summary_line.monto_neto,
                    "mntExo":summary_line.monto_exo,
                    "mntExe":summary_line.monto_exe,
                    "mntExp":summary_line.monto_exe,
                    "mntGrat":summary_line.monto_grat,
                    "mntOtros":summary_line.monto_otros
                })
            record.resumen_diario_json = json.dumps(resumen_diario_json, indent=4)
    
    def enviar_resumen_diario(self):
        if not self.company_id:
            raise UserError("La campo de compañía es Obligatoria.")
        tipo_envio = self.company_id.tipo_envio
        endpoint = self.company_id.endpoint
        data = json.loads(self.resumen_diario_json)
        token = generate_token_by_company(self.company_id,100000)
        response_env = enviar_doc_resumen_url(endpoint,data,token,tipo_envio)
        response = response_env.json()
        self.json_respuesta = json.dumps(response, indent=4)
        #os.system("echo '%s'"%(json.dumps(response_env.json(), indent=4)))
        if response_env.ok:
            result = response.get("result",False)
            if result:
                if result.get("data",False):
                    data = result["data"]
                    self.estado_emision = data['estadoEmision']
                    self.estado = "enviado"
                    self.ticket = data['idTicket']
            return True, ""
        else:
            recepcionado, estado_emision, msg_error = extraer_error(response)
            if recepcionado:
                self.estado_emision = estado_emision
                return True, msg_error
            else:
                return False, msg_error


    def consulta_estado_resumen(self):
        token = generate_token_by_company(self.company_id,100000)
        endpoint = self.company_id.endpoint
        data={
            "method":"Factura.consultaResumen",
            "kwargs":{
                "data":{
                    "ticket":self.ticket,
                    "ruc":self.company_id.vat,
                    "tipoEnvio":int(self.company_id.tipo_envio),
                }
            }
        }
        #os.system("echo '%s'"%(json.dumps(data)))
        #os.system("echo '%s'"%(endpoint))
        #os.system("echo '%s'"%(token))
        headers = { 
            "Content-Type": "application/json",
            "Authorization" : token
        }
        r = requests.post(endpoint, headers=headers, data=json.dumps(data))

        response = r.json()
        result = response.get("result",False)
        if result:
            status =  result.get("status",False)
            if status:
                print(response)
                os.system("echo '%s'"%(response))
                self.estado_emision = response["result"]["status"]
            else:
                raise UserError(json.dumps(response))
        else:
            raise UserError(json.dumps(response))
        

   