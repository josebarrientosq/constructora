 # -*- coding: utf-8 -*-
from odoo import fields,models,api,exceptions, _
from odoo.exceptions import UserError,ValidationError
import datetime
from odoo.tools import float_is_zero, float_compare
from odoo.http import request
from ..auth import oauth
from ..auth.oauth import consulta_estado_comprobante
import os
import site
import json
import ast
import base64
import io

TYPE2JOURNAL = {
    'out_invoice': 'sale',
    'in_invoice': 'purchase',
    'out_refund': 'sale',
    'in_refund': 'purchase',
}
class AccountInvoice(models.Model):
    _inherit = "account.invoice"
    _name = ""

    account_log_status_ids = fields.One2many("account.log.status","account_invoice_id",string="Registro de Envíos",copy=False)

    partner_id = fields.Many2one(
                            'res.partner', 
                            string='Partner',
                            change_default=True,
                            readonly=True,
                            states={'draft': [('readonly', False)]},
                            track_visibility='always')

    json_comprobante = fields.Text(string="JSON Comprobante",copy=False)
    json_respuesta = fields.Text(string="JSON Respuesta",copy=False)
    digest_value = fields.Char(string="Digest Value",copy=False)
    status_envio = fields.Boolean(
        string="Estado del envio del documento", 
        default=False,
        copy=False
        )
    status_baja = fields.Boolean(
        string="Estado de la baja del documento", 
        default=False,
        copy=False
        )
    #variables para notas de venta
    sustento_nota = fields.Text(
        string="Sustento de nota",
        readonly=True,
        states={
            'draft': [
                ('readonly', False)
                ]
            },
        copy=False)

    tiene_guia_remision = fields.Boolean("Tienes guía de Remisión",default=False,copy=False)
    stock_picking_id = fields.Many2one("stock.picking",string="Documento de Envío",copy=False)
    numero_guia = fields.Char("Número de Guía",related="stock_picking_id.numero_guia",copy=False)
    
    tipo_operacion = fields.Selection(selection=[("01","Venta Interna"),("02","Exportación"),("04","Venta Interna - Anticipos"),("05","Venta Itinerante")],default="01",required=True)

    @api.onchange("tiene_guia_remision")
    def _set_default_tiene_guia_remision(self):
        for record in self:
            if not record.tiene_guia_remision:
                record.stock_picking_id = False
                record.numero_guia = False

    #tipo_nota
    reference_id = fields.Many2one(
        'account.invoice',
        string='Documento de Referencia', 
        change_default=True,
        readonly=True,
        states={
            'draft': [
                ('readonly', False)
                ]},
        track_visibility='always')
    tipo_nota_credito = fields.Many2one(
        'einvoice.catalog.09',
        string='Tipo de Nota de Credito',
        readonly=True,
        states={
            'draft': [
                ('readonly', False)
                ]})
    tipo_nota_dedito = fields.Many2one(
        'einvoice.catalog.10',
        string='Tipo de Nota de Debito',
        readonly=True,
        states={
            'draft': [
                ('readonly', False)
                ]})
    documento_baja_id = fields.Many2one("efact.account_comunicacion_baja",copy=False)
    documento_baja_status = fields.Selection(string="Status del Documento de Baja",related="documento_baja_id.estado_emision",copy=False)

    #baja_id = fields.Many2one('facturactiva.baja_documento', string='Documento de baja perteneciente',
    #   ondelete='cascade', index=True)
    tipo_cambio_fecha_factura = fields.Float(
        string="Tipo de cambio a la fecha de factura", 
        default=1.0)
    descuento_global = fields.Float(
        string="Descuento Global (%)", 
        readonly=True, 
        states={'draft': [('readonly', False)]}, 
        default=0.0)
    date_invoice = fields.Date(string='Invoice Date',
                               readonly=True, 
                               index=True,
                               help="Keep empty to use the current date", 
                               default=datetime.datetime.now())
    
    @api.multi
    def actualizar_datos_cliente(self):
        for record in self:
            if record.partner_id:
                record.partner_id.update_document()
            else:
                raise UserError("No hay cliente asociado a la factura")

    @api.multi
    def get_taxes_values(self):
        tax_grouped = {}
        for line in self.invoice_line_ids:
            if len(line.invoice_line_tax_ids)>0:
                if line.invoice_line_tax_ids[0].tipo_afectacion_igv.code=="10":
                    price_unit = line.price_unit * (1 - (line.discount or 0.0) / 100.0)-line.descuento_unitario
                    taxes = line.invoice_line_tax_ids.compute_all(price_unit, self.currency_id, line.quantity, line.product_id, self.partner_id)['taxes']
                    for tax in taxes:
                        val = self._prepare_tax_line_vals(line, tax)
                        key = self.env['account.tax'].browse(tax['id']).get_grouping_key(val)

                        if key not in tax_grouped:
                            tax_grouped[key] = val
                        else:
                            tax_grouped[key]['amount'] += val['amount']
                            tax_grouped[key]['base'] += val['base']
        return tax_grouped

    @api.model
    def default_get(self, fields_list):
        res = super(AccountInvoice, self).default_get(fields_list)
        journal_id=self.env['account.journal'].search(
            [
                ['invoice_type_code_id', '=', self._context.get("type_code")]
            ], 
            limit=1)

        #payment_term_id = self.env["account.payment.term"].sudo().browse([self._context.get("default_payment_term_id")])
        #os.system("echo '%s'"%(json.dumps(self._context)))
        res["journal_id"] = journal_id.id
        #res["payment_term_id"]=payment_term_id.id
        return res

    @api.depends('state', 'journal_id')
    def _get_sequence_number_next(self):
        """ computes the number that will be assigned to the first invoice/bill/refund of a journal, in order to
        let the user manually change it.
        """
        for invoice in self:
            journal_sequence, domain = invoice._get_seq_number_next_stuff()
            if (invoice.state == 'draft') and not self.search(domain, limit=1):
                number_next = journal_sequence._get_current_sequence().number_next_actual
                invoice.sequence_number_next = '%%0%sd' % journal_sequence.padding % number_next
            else:
                invoice.sequence_number_next = ''
   
    """
    def _default_invoice_type_code(self):
        if self.journal_id:
            return self.journal_id.invoice_type_code_id
    """
    invoice_type_code = fields.Selection(
        string="Tipo de Comprobante",
        store=True,
        related="journal_id.invoice_type_code_id")

    @api.one
    @api.depends(
        'invoice_line_ids.price_subtotal',
        'tax_line_ids.amount','currency_id','company_id','date_invoice',
        'type','descuento_global')
    def _compute_total_venta(self):

        self.total_venta_gravado=sum(
            [
                line.price_subtotal 
                    for line in self.invoice_line_ids 
                        if len(
                            [line.price_subtotal  for line_tax in line.invoice_line_tax_ids 
                                if line_tax.tipo_afectacion_igv.code in ["10"] ] )
            ])*(100 - self.descuento_global)/100
        
        self.total_venta_inafecto = sum(
            [
                line.price_subtotal 
                    for line in self.invoice_line_ids 
                        if len(
                            [line.price_subtotal  for line_tax in line.invoice_line_tax_ids 
                                if line_tax.tipo_afectacion_igv.code in ["40","30"] ] )
            ]
        )
        self.total_venta_exonerada = sum(
            [
                line.price_subtotal 
                    for line in self.invoice_line_ids 
                        if len(
                            [line.price_subtotal  for line_tax in line.invoice_line_tax_ids 
                                if line_tax.tipo_afectacion_igv.code in ["20"] ] )
            ]
        )
        self.total_venta_gratuito = sum(
            [
                line.product_id.lst_price*line.quantity
                    for line in self.invoice_line_ids 
                       if len(
                            [line.price_subtotal  for line_tax in line.invoice_line_tax_ids 
                                if line_tax.tipo_afectacion_igv.code in ["31","32","33","34","35","36"] ] )
            ]
        )
        self.total_descuentos = sum(
            [
                (line.price_subtotal*line.discount/100) +line.descuento_unitario
                    for line in self.invoice_line_ids
            ]
        )
        self.total_descuento_global = (
            (self.total_venta_gravado+self.amount_tax)/(1-self.descuento_global/100))*self.descuento_global/100

    total_tax_discount = fields.Monetary(
        string="Total Descuento Impuesto",
        default=0.0,
        compute="_compute_total_venta")
    total_venta_gravado=fields.Monetary(
        string="Gravado",
        default=0.0,
        compute="_compute_total_venta")
    total_venta_inafecto=fields.Monetary(
        string="Inafecto",
        default=0.0,
        compute="_compute_total_venta")
    total_venta_exonerada=fields.Monetary(
        string="Exonerado",
        default=0.0,
        compute="_compute_total_venta")
    total_venta_gratuito=fields.Monetary(
        string="Gratuita",
        default=0.0,
        compute="_compute_total_venta")
    total_descuentos=fields.Monetary(
        string="Total Descuentos",
        default=0.0,
        compute="_compute_total_venta")
    total_descuento_global = fields.Monetary(
        string="Total Descuentos Global",
        default=0.0,
        compute="_compute_total_venta")

    @api.one
    @api.depends(
        'invoice_line_ids.price_subtotal','tax_line_ids.amount', 
        'currency_id','company_id','date_invoice',
        'type','descuento_global','total_tax_discount'
        )
    def _compute_amount(self):
        round_curr = self.currency_id.round
        self.amount_untaxed = sum(line.price_subtotal for line in self.invoice_line_ids)
        self.total_tax_discount = sum(round_curr(line.amount) for line in self.tax_line_ids)*self.descuento_global/100
        self.amount_tax = sum(round_curr(line.amount) for line in self.tax_line_ids)
        self.amount_total = self.amount_untaxed + self.amount_tax
        amount_total_company_signed = self.amount_total
        amount_untaxed_signed = self.amount_untaxed
        if self.currency_id and self.company_id and self.currency_id != self.company_id.currency_id:
            currency_id = self.currency_id.with_context(
                date=self.date_invoice
            )
            amount_total_company_signed = currency_id.compute(
                self.amount_total, self.company_id.currency_id
            )
            amount_untaxed_signed = currency_id.compute(
                self.amount_untaxed, self.company_id.currency_id
            )
        sign = self.type in ['in_refund', 'out_refund'] and -1 or 1
        self.amount_total_company_signed = amount_total_company_signed * sign
        self.amount_total_signed = self.amount_total * sign
        self.amount_untaxed_signed = amount_untaxed_signed * sign

    @api.model
    def _default_journal(self):
        if self._context.get('default_journal_id', False):
            #print "DEFAULT JOURNAL ID"+str(self._context.get('default_journal_id'))
            obj = self.env['account.journal'].browse(
                self._context.get('default_journal_id')
            )
            #print str(obj)
            return obj
        inv_type = self._context.get('type', 'out_invoice')
        inv_types = inv_type if isinstance(inv_type, list) else [inv_type]
        #AGREGADO
        invoice_type_code = self._context.get('type_code', '01')
        #FIN AGREGADO
        company_id = self._context.get(
            'company_id', 
            self.env.user.company_id.id
        )
        domain = [
            #('type', 'in', filter(None, map(TYPE2JOURNAL.get, inv_types))),
            ('company_id', '=', company_id),
            ('invoice_type_code_id', '=', invoice_type_code),
        ]
        obj = self.env['account.journal'].search(domain, limit=1)
        #for o in self.env['account.journal'].search(domain):
        #    print o
        return obj
    """
    @api.model
    def _default_payment_term_id(self):
        print self._context.get('default_payment_term_id')
        if self._context.get('default_payment_term_id', False):
            
            return self.env['account.payment.term'].browse(
                self._context.get('default_payment_term_id')
            )
    payment_term_id = fields.Many2one(
        'account.payment.term',
        string='Payment Terms',
        oldname='payment_term',
        states={'draft': [('readonly', False)]},
        help="Si usas términos de pago,the due date will be \
            computed automatically at the generation \
            of accounting entries. If you keep the payment \
            term and the due date empty, it means direct payment. \
            The payment term may compute several due dates,\
            for example 50% now, 50% n one month.",
        default=_default_payment_term_id)
    """

    estado_emision = fields.Selection(
        selection=[
            ('A', 'Aceptado'),
            ('E', 'Enviado a SUNAT'),
            ('N', 'Envio Erróneo'),
            ('O', 'Aceptado con Observación'),
            ('R', 'Rechazado'),
            ('P', 'Pendiente de envió a SUNAT'),
        ],
        string="Estado Emisión a SUNAT",
        readonly=True,
        copy=False
    )

    @api.model
    def _default_new_invoice(self):
        return self._context.get('default_new_invoice', True)

    new_invoice = fields.Boolean(
        string="Indica si es nuevo o proviene de un documento anterior",
        default=_default_new_invoice)
        
    @api.multi
    def envio_documento_batch(self):
        documets = self.env['account.invoice'].search([['status_envio', '=', False]])
        nro_exitos = 0
        nro_errores = 0
        for document in documets:
            result, msg = oauth.enviar_doc(document,document.company_id.endpoint)
            if result:
                nro_exitos = nro_exitos + 1
            else:
                nro_errores = nro_errores + 1
        # talvez se necesite una recursiva
        print("EXITOS :" + str(nro_exitos) + " ERRORES:" + str(nro_errores))

    @api.multi
    def generar_nota_debito(self):
        if not self.number:
            self.action_invoice_open()
        ref = request.env.ref("account.invoice_form")
        inv_lines2 = []
        for il1 in self.invoice_line_ids:
            obj = il1.copy(default={
                "invoice_id": ""
            })
            inv_lines2.append(obj.id)
        print( str(self.number[0:4]) + " - " + str(int(self.number[5:len(self.number)])))
        return {
            "type": "ir.actions.act_window",
            "res_model": "account.invoice",
            "target": "self",
            "view_id": ref.id,
            "view_mode": "form",
            "context": {
                'default_partner_id': self.partner_id.id,
                'default_refund_invoice_id': self.id,
                'default_date_invoice': self.date_invoice,
                'default_payment_term_id': self.payment_term_id.id,
                'default_invoice_line_ids': inv_lines2,
                'default_new_invoice': False,
                'type': 'out_invoice',
                'journal_type': 'sale',
                'type_code': '08',
                'default_number': 'Nota de Debito'},
            "domain": [('type', 'in', ('out_invoice', 'out_refund')), ('journal_id.invoice_type_code_id', '=', '08')]
        }

    @api.multi
    def generar_nota_credito(self):
        if not self.number:
            self.action_invoice_open()
        ref = request.env.ref("account.invoice_form")
        inv_lines2 = []
        for il1 in self.invoice_line_ids:
            obj = il1.copy(default={
                "invoice_id": ""
            })
            inv_lines2.append(obj.id)
        print(str(self.number[0:4]) + " - " + str(int(self.number[5:len(self.number)])))
        return {
            "type": "ir.actions.act_window",
            "res_model": "account.invoice",
            "target": "self",
            "view_id": ref.id,
            "view_mode": "form",
            "context": {
                'default_partner_id': self.partner_id.id,
                'default_refund_invoice_id': self.id,
                'default_date_invoice': self.date_invoice,
                'default_payment_term_id': self.payment_term_id.id,
                'default_invoice_line_ids': inv_lines2,
                'default_new_invoice': False,
                'default_type': 'out_refund',
                'journal_type': 'sale',
                'type_code': '07',
                'default_number': 'Nota de Credito'},
            "domain": [('type', 'in', ('out_invoice', 'out_refund')), ('journal_id.invoice_type_code_id', '=', '07')]
        }

    '''
    @api.multi
    def enviar_nota(self):
        result, msg = oauth.enviar_doc(self)
        #print msg
        if not result:
            #self.write({'state': 'cancel'})
            ref = request.env.ref("account.invoice_form")
            
            inv_lines=self.invoice_line_ids
            inv_lines2=[]
            for il1 in inv_lines:
                obj = il1.copy(default={
                    "invoice_id": ""
                })
                inv_lines2.append(obj.id)

            accion = {
                "type": "ir.actions.act_window",
                "res_model": "account.invoice",
                "target": "self",
                "view_id": ref.id,
                "view_mode": "form",
                "context": {
                    'default_partner_id': self.partner_id.id,
                    'default_date_invoice': self.date_invoice,
                    'default_payment_term_id': self.payment_term_id.id,
                    'default_invoice_line_ids': inv_lines2,
                    'default_journal_id': self.journal_id.id,
                    'type': 'out_invoice',
                    'journal_type': 'sale',
                    'type_code': self.invoice_type_code,

                },
                "domain": [('type', 'in', ('out_invoice', 'out_refund')),
                           ('journal_id.invoice_type_code_id', '=', self.invoice_type_code)]
            }

            #return ast.literal_eval(accion)
            
            print accion
            return {
                'name': 'Message',
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'custom.pop.message',
                'target': 'new',
                'context': {
                    'default_name' : msg
                }
            }

        return obj
    '''

    def dar_baja_document(self):
        ref = self.env.ref("efact.view_comunicacion_baja_form")
        """
        return {
            "name":"Comunicación de Baja",
            "type": "ir.actions.act_window",
            "res_model": "efact.account_comunicacion_baja",
            "target": "self",
            "views": [[ref.id,"form"]]
        }
        """
        if self.documento_baja_id:
            return {
                "type": "ir.actions.act_window",
                "res_model": "efact.account_comunicacion_baja",
                "target": "self",
                "view_id": ref.id,
                "view_mode": "form",
                "res_id":self.documento_baja_id.id
            }
        else:
            return {
                "type": "ir.actions.act_window",
                "res_model": "efact.account_comunicacion_baja",
                "target": "self",
                "view_id": ref.id,
                "view_mode": "form",
                "context": {
                    'default_invoice_ids': [self.id],
                    'default_invoice_type_code_id': self.invoice_type_code,
                    'default_date_invoice': datetime.datetime.now().strftime("%Y-%m-%d")
                }
            }
        
        #"domain": [('type', 'in', ('out_invoice', 'out_refund')), ('journal_id.invoice_type_code_id', '=', '07')]

    def validacion_factura(self):
        errors= []
        if self.partner_id.company_type != "company":
            errors.append('''* El cliente seleccionado debe ser de tipo Compañía para las facturas
                            Recuerda: que para un cliente de tipo compañía, los campos de tipo de documento,
                            Documento y Razón Social son Obligatorios. Además el tipo de Documento debe ser RUC.''')    
        if self.partner_id.catalog_06_id.code!="6":
            errors.append("* El cliente seleccionado debe tener como tipo de documento el RUC, esto es necesario para facturas.")
        if not self.partner_id.vat:
            errors.append("* El cliente selecionado no tiene RUC, esto es necesario para facturas")
        elif len(self.partner_id.vat)!=11:
                errors.append("* El RUC del cliente selecionado debe tener 11 dígitos")
        if not self.partner_id.zip:
            errors.append("* El cliente selecionado no tiene configurado el Ubigeo.")
        """
        if not self.partner_id.email:
            errors.append("* El cliente selecionado no tiene email.")
        """
        for line in self.invoice_line_ids:
            if len(line.invoice_line_tax_ids)==0:
                errors.append("* El Producto debe tener al menos un tipo de impuesto Asociado")
            for tax in line.invoice_line_tax_ids:
                if not tax.tipo_afectacion_igv:
                    errors.append("* El Tipo de Afectacion al IGV no esta configurado para el Impuesto %s del item %s" % (tax.name,line.name))
            #Falta Validar los tipos de Afectación al IGV
            if not line.uom_id.code:
                errors.append("* La Unidad de Medida seleccionada para el item %s no tiene código" % (line.name))

        return errors

    def validacion_boleta(self):
        errors=[]
        """
        if not self.partner_id.email:
            errors.append("* El cliente selecionado no tiene email.")
        """
        return errors

    def validar_datos_compania(self):
        errors = []
        if not self.company_id.partner_id.vat:
            errors.append("* No se tiene configurado el RUC de la empresa emisora")

        if not self.company_id.partner_id.catalog_06_id:
            errors.append("* No se tiene configurado el tipo de documento de la empresa emisora")
        elif self.company_id.partner_id.catalog_06_id.code != '6':
            errors.append("* El Tipo de Documento de la empresa emisora debe ser RUC")

        if not self.company_id.partner_id.zip:
            errors.append("* No se encuentra configurado el Ubigeo de la empresa emisora.")

        if not self.company_id.partner_id.street:
            errors.append("* No se encuentra configurado la dirección de la empresa emisora.")
        
        if not self.company_id.partner_id.registration_name:
            errors.append("* No se encuentra configurado la Razón Social de la empresa emisora.")

        return errors

    def validar_venta_gratuita(self):
        errors=[]
        for line in self.invoice_line_ids:
            if len(line.invoice_line_tax_ids)>0:
                if line.invoice_line_tax_ids[0].tipo_afectacion_igv.code in ["31","32","33","34","35","36"] and line.price_unit!=0:
                    errors.append("* El producto %s debe tener como precio unitario el valor de 0 debido a que es una venta gratuita"%(line.name))
        return errors
    
    def validar_diario(self):
        errors = []
        if self.journal_id.tipo_envio != self.company_id.tipo_envio:
            errors.append("* El tipo de envío configurado en la compañía debe coincidir con el tipo de envío del Diario que ha seleccionado.")
        return errors

    def validar_guia_remision(self):
        errors = []
        if self.stock_picking_id:
            if not self.stock_picking_id.numero_guia:
                errors.append("* La operación de envío asociada no tiene un Número de guía.")

    def validar_comprobante(self):
        self.action_invoice_open()
        self.get_comprobante()

    def action_invoice_open(self):
        if self.type == "in_invoice":
            if self.reference:
                self._validar_reference(self)
            else:
                raise UserError("La Referencia de Proveedor de la Factura de compra es obligatoria")
            return super(AccountInvoice, self).action_invoice_open()
        
        if self.journal_id.formato_comprobante == 'fisico':
            obj = super(AccountInvoice, self).action_invoice_open()
            return obj
        #Validaciones cuando el comprobante es factura
        msg_error=self.validar_datos_compania()
        msg_error+=self.validar_venta_gratuita()
        msg_error+=self.validar_diario()
        
        if self.journal_id.invoice_type_code_id=="01":
            msg_error += self.validacion_factura()
            if len(msg_error)>0:
                msg = "\n\n".join(msg_error)
                raise UserError(msg)

        if self.journal_id.invoice_type_code_id=="03":
            msg_error += self.validacion_boleta()
            if len(msg_error)>0:
                msg = "\n\n".join(msg_error)
                raise UserError(msg)

        if self.partner_id.catalog_06_id.code!="6" and self.journal_id.invoice_type_code_id=="01":
            raise UserError("Tipo de documento del receptor no valido")

        """
        if (datetime.datetime.now()-datetime.datetime.strptime(self.date_invoice, "%Y-%m-%d")).days > 7:
            raise UserError("No se puede emitir luego de 7 días")
        """
        obj = super(AccountInvoice, self).action_invoice_open()

        self.write({'tipo_cambio_fecha_factura': oauth.get_tipo_cambio(self, 2) if self.currency_id.name == 'USD' else 1.0 })
        
        result, msg = oauth.enviar_doc(self,self.company_id.endpoint)
        if self.type=="out_invoice":
            # proceso de generar
            
            if result:
                if msg=="":
                    return obj
                else: #RECIBIDO PERO CON ERROR
                    self.action_invoice_cancel()
                    return {
                        'name': 'Message',
                        'type': 'ir.actions.act_window',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'custom.pop.message',
                        'target': 'new',
                        'context': {
                            'default_name': msg
                        }
                    }
            else: #NO RECIBIDO CON ERROR
                return {
                    'name': 'Message',
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'custom.pop.message',
                    'target': 'new',
                    'context': {
                        'default_name': msg
                    }
                }
        return obj


    @api.model
    def _validar_reference(self, obj):
        reference = obj.reference
        if reference:
            if len(reference)==13:
                if reference[4:5]=="-" and reference[5:13].isdigit():
                    return True
                else:
                    raise UserError("La referencia debe tener el formato XXXX-########")
            else:
                raise UserError("La referencia debe tener el formato XXXX-########")
        else:
            raise UserError("Debe colocar la Referencia del proveedor")
    @api.model
    def create(self, vals):
        if 'origin' in vals:
            sale_order=self.env["sale.order"].search([["name","=",vals['origin']]])
            #vals["invoice_type_code_id"]=sale_order["tipo_documento"]
            vals["invoice_type_code_id"] = sale_order["tipo_documento"]
            journal_ids = self.env["account.journal"].search(
                [
                    ["invoice_type_code_id", "=", sale_order["tipo_documento"]]
                ]
            )

            if len(journal_ids) > 0 and sale_order["tipo_documento"]:
                vals["journal_id"] = journal_ids[0].id
        obj = super(AccountInvoice, self).create(vals)
        if obj.type == "in_invoice":
            self._validar_reference(obj)
        if not obj.new_invoice:
            obj.invoice_line_ids = [
                invoice_line for invoice_line in vals["invoice_line_ids"]
            ]
        return obj

    @api.multi
    def action_invoice_sent(self):
        """ Open a window to compose an email, with the edi invoice template
            message loaded by default
        """
        self.ensure_one()
        template = self.env.ref('account.email_template_edi_invoice', False)
        compose_form = self.env.ref('mail.email_compose_message_wizard_form', False)
        ctx = dict(
            default_model='account.invoice',
            default_res_id=self.id,
            default_use_template=bool(template),
            default_template_id=template and template.id or False,
            default_composition_mode='comment',
            mark_invoice_as_sent=True,
            custom_layout="account.mail_template_data_notification_email_account_invoice"
        )

        fname=self.number+".xml"
        if len(self.account_log_status_ids)>0:
            log_status = self.account_log_status_ids[-1]
            data_signed_xml = log_status.signed_xml_data 
            if data_signed_xml:
                datas= base64.b64encode(data_signed_xml.encode())
                ctx["default_attachment_ids"]=[self.env["ir.attachment"].create({"name":fname,"type":"binary","datas":datas,"mimetype":"text/xml","datas_fname":fname}).id]            

        return {
            'name': _('Compose Email'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form.id, 'form')],
            'view_id': compose_form.id,
            'target': 'new',
            'context': ctx,
        }
    
    def get_comprobante(self):
        if self.json_respuesta:
            json_respuesta=json.loads(self.json_respuesta)
            if "result" in json_respuesta:
                if "request_id" in json_respuesta["result"]:
                    res = consulta_estado_comprobante(self.company_id.endpoint,
                                                        json_respuesta["result"]["request_id"],
                                                        self.company_id.api_key,
                                                        self.company_id.api_secret)
                    res = res.text
                    res = json.loads(res)
                    data = {}
                    if "result" in res:
                        if "signed_xml_data" in res["result"]:
                            data["signed_xml_data"] = res["result"]["signed_xml_data"]
                        if "user_id" in res["result"]:
                            data["api_user_id"] = res["result"]["user_id"]
                        if "content_xml" in res["result"]:
                            data["content_xml"] = res["result"]["content_xml"]
                        if "response_xml" in res["result"]:
                            data["response_xml"] = res["result"]["response_xml"]
                        if "request_json" in res["result"]:
                            data["request_json"] = res["result"]["request_json"]
                        if "response_json" in res["result"]:
                            data["response_json"] = res["result"]["response_json"]
                            response_json = res["result"]["response_json"]
                            if "data" in response_json:
                                if "request_id" in response_json:
                                    data["api_request_id"] = response_json["request_id"]

                                if "data" in response_json:
                                    if "tipoDocumento" in response_json["data"]:
                                        tipo_documento = response_json["data"]["tipoDocumento"]
                                        if tipo_documento == '01':
                                            data["name"] = "Factura electrónica "+self.number
                                        elif tipo_documento == '03':
                                            data["name"] = "Boleta Electrónica "+self.number
                                        elif  tipo_documento == '07':
                                            data["name"] = "Nota de Crédito "+self.number
                                        elif tipo_documento == '08':
                                            data["name"] = "Nota de Débito "+self.number

                                    if "estadoEmision" in response_json["data"]:
                                        data["status"] = response_json["data"]["estadoEmision"]

                        if "signed_xml" in res["result"]:
                            data["signed_xml"] = res["result"]["signed_xml"]
                        if "unsigned_xml" in res["result"]:
                            data["unsigned_xml"] = res["result"]["unsigned_xml"]
                        if "status" in res["result"]:
                            data["status"] = res["result"]["status"]
                            
                    self.account_log_status_ids = [(0,0,data)]
                    #os.system("echo '%s'"%(res))

    @api.multi
    def invoice_print(self):
        """ Print the invoice and mark it as sent, so that we can see more
            easily the next step of the workflow
        """
        self.ensure_one()
        self.sent = True
        return self.env.ref('account.account_invoices').report_action(self)

class CustomPopMessage(models.TransientModel):
    _name = "custom.pop.message"
    name = fields.Char('Message')
    accion = fields.Text(string="Accion a realizar")
