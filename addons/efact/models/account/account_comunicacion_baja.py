from odoo import fields, models, api
from odoo.exceptions import UserError, ValidationError
from lxml import etree
from datetime import datetime,timedelta
from ..auth import oauth

class AccountComunicacionBaja(models.Model):
    _name = "efact.account_comunicacion_baja"
    _description = "Documento de baja"
    #_inherit = "account.invoice"

    date_invoice = fields.Date(string='Invoice Date',
                               readonly=True, states={'draft': [('readonly', False)]}, index=True,
                               help="Keep empty to use the current date", required=True, 
                               copy=False, 
                               default=datetime.now())

    @api.multi
    def name_get(self):
        result = []
        for record in self:
            fecha = datetime.strptime(self.date_invoice, "%Y-%m-%d").strftime("%Y%m%d")
            name = 'RA-' + fecha +"-"+str(record.contador)
            result.append((record.id, name))
        return result
    
    @api.constrains("date_invoice")
    def _validar_fecha(self):
       # if (datetime.datetime.now()-datetime.datetime.strptime(self.date_invoice, "%Y-%m-%d")).days > 7:
       #print str(datetime.datetime.now())+" || "+ str(datetime.datetime.strptime(self.date_invoice, "%Y-%m-%d"))
       if((datetime.now() - timedelta(7)>datetime.strptime(self.date_invoice, "%Y-%m-%d")) or datetime.strptime(self.date_invoice, "%Y-%m-%d")>datetime.now()):
            raise ValidationError("No se puede enviar documentos con mas de 7 dias de antiguedad")

    state = fields.Selection([
        ('draft', 'Draft'),
        ('sent', 'Sent'),
        ('cancel', 'Cancelled'),
    ], string='Status', index=True, readonly=True, default='draft',
        track_visibility='onchange', copy=False)

    sent = fields.Boolean(readonly=True, default=False, copy=False,
                          help="It indicates that the invoice has been sent.")

    @api.model
    def _default_documents(self):
        if self._context.get('default_documents', False):
            return self.env['account.invoice'].browse(self._context.get('default_documents'))

    #documents = fields.Many2one('account.invoice', string='Documentos de Baja'
    #                            , readonly=True, states={'draft': [('readonly', False)]}, required=True, default=_default_documents
    #                            )

    #document_line_ids = fields.One2many('facturactiva.baja_documento.line', 'baja_documento_id', string='Documentos de baja',readonly=True, states={'draft': [('readonly', False)]}, copy=True)

    user_id = fields.Many2one('res.users', string='Salesperson', track_visibility='onchange',
                              readonly=True, states={'draft': [('readonly', False)]}, required=True,
                              default=lambda self: self.env.user)

    company_id = fields.Many2one('res.company', string='Company', change_default=True,
                                 required=True, readonly=True, states={'draft': [('readonly', False)]})

    company_currency_id = fields.Many2one('res.currency', related='company_id.currency_id', string="Company Currency",
                                          readonly=True)
    status_envio = fields.Boolean("Estado del envio del documento", default=False)
    json_comprobante = fields.Text(string="JSON de envio")
    
    json_respuesta = fields.Text(string="JSON de respuesta")

    estado_emision = fields.Selection([
        ('A', 'Aceptado'),
        ('E', 'Enviado a SUNAT'),
        ('N', 'Envio Erroneo'),
        ('O', 'Aceptado con Observacion'),
        ('R', 'Rechazado'),
        ('P', 'Pendiente de envio a SUNAT'),
    ], string="Estado Emision a SUNAT", readonly=True)

    @api.one
    def default_contador(self):
        cont = 1
        n = 0
        documets = self.env['efact.account_comunicacion_baja'].search_read(
            [['state', '=', 'sent'], ['date_invoice', '=', self.date_invoice]],{
                'order':'DESC contador'
            })
        return documets[0].contador + 1

    contador = fields.Integer(string="Contador", states={'draft': [('readonly', False)]}, default=default_contador)



    motivo = fields.Text(string="Motivo de Baja", readonly=True, states={'draft': [('readonly', False)]}, required=True)

    company_id = fields.Many2one('res.company', string='Company', change_default=True,
                                 required=True, readonly=True, states={'draft': [('readonly', False)]},
                                 default=lambda self: self.env['res.company']._company_default_get('efact.account_comunicacion_baja'))

    @api.multi
    def invoice_validate(self):
        for invoice in self:
            if self.search([('id', '!=', invoice.id)]):
                raise UserError("Duplicated document.")
        return self.write({'state': 'sent'})


    @api.multi
    def action_invoice_sent(self):
        # lots of duplicate calls to action_invoice_open, so we remove those already open
        to_open_invoices = self.filtered(lambda inv: inv.state != 'sent')
        if to_open_invoices.filtered(lambda inv: inv.state not in ['draft']):
            raise UserError("Invoice must be in draft or Pro-forma state in order to validate it.")
        #to_open_invoices.action_move_create()
        #to_open_invoices.invoice_validate()
        envio, msg = oauth.baja_doc(self)
        if envio:
            print(msg)
            if msg=="":
                for document in self.invoice_ids:
                    obj = self.env['account.invoice'].browse(document.id)
                    #obj.write({'state': 'cancel'})
                    obj.action_invoice_cancel()
                    obj.write({'status_baja': True})
            self.write({'state': 'sent'})
        if msg!="":
            return {
                'name': 'Message',
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'custom.pop.message',
                'target': 'new',
                'context': {
                    'default_name': msg
                }
            }
        return self

    @api.multi
    def action_invoice_cancel(self):
        if self.filtered(lambda inv: inv.state not in ['draft','open']):
            raise UserError("Invoice must be in draft or sent state in order to be cancelled.")
        return self.write({'state': 'cancel'})

    @api.multi
    def action_invoice_draft(self):
        if self.filtered(lambda inv: inv.state != 'cancel'):
            raise UserError("Invoice must be cancelled in order to reset it to draft.")
        # go from canceled state to draft state
        return self.write({'state': 'draft', 'date': False})


    @api.onchange('date_invoice')
    def onchange_calcute_cont(self):
        cont = 1
        n=0
        documets = self.env['efact.account_comunicacion_baja'].search([['state', '=', 'sent'],['date_invoice', '=', self.date_invoice]])
        for document in documets:
            if cont < document.contador:
                cont = document.contador
            n = n +1
        if n!=0:
            self.contador = cont + 1
        else:
            self.contador = 1

    def _default_invoice_ids(self):
        invoices = []
        if self._context.get('default_invoice_ids', False):
            for document in self._context.get('default_invoice_ids'):
                invoices.append(self.env['account.invoice'].browse(document))
            return invoices

    invoice_ids = fields.One2many('account.invoice',"documento_baja_id",default=_default_invoice_ids)

    def _list_invoice_type(self):
        catalogs=self.env["einvoice.catalog.01"].search([])
        list=[]
        for cat in catalogs:
            list.append((cat.code,cat.name))
        return list

    def _default_invoice_type_code_id(self):
        if self._context.get('default_invoice_type_code_id', False):
            return self._context.get('default_invoice_type_code_id')

    invoice_type_code_id = fields.Selection(string="Tipo de Documento", selection=_list_invoice_type, default=_default_invoice_type_code_id,readonly=True, states={'draft': [('readonly', False)]},)


    def generar_comunicacion_baja(self):
        tipo_resumen="RA"
        now = datetime.now()
        data = {
            "tipoResumen": tipo_resumen,
            "fechaGeneracion": self.date_invoice,
            "idTransaccion": str(self.id),
            "resumen": {
                "id": self.contador,
                "tipoDocEmisor": self.company_id.partner_id.catalog_06_id.code,
                "numDocEmisor": self.company_id.partner_id.vat,
                "nombreEmisor": self.company_id.partner_id.registration_name,
                "fechaReferente": self.date_invoice,
                "tipoFormatoRepresentacionImpresa": "GENERAL"
            }
        }

        data_detalle = []
        for document in self.invoice_ids:
            data_detalle.append({
                "serie": document.number[0:4],
                "correlativo": int(document.number[5:len(document.number)]),
                "tipoDocumento": document.invoice_type_code,
                "motivo": self.motivo
            })

        data['detalle'] = data_detalle

        return data