#!/usr/bin/env python2
import requests
from odoo.exceptions import UserError, ValidationError
import json
import os
import datetime
from bs4 import BeautifulSoup
import jwt
import time
from ..utils.number_to_letter import to_word
import re

def generate_token_by_company(company_id,time_valid=10):
    api_key = company_id.api_key
    api_secret = company_id.api_secret
    headers = {
        "iss": api_key
    }
    payload = {
        "exp": int(time.time()) + time_valid,
    }
    token = jwt.encode(payload, api_secret, 'HS256', headers)
    return token

def generate_token(api_key, api_secret, time_valid=10):
    headers = {
        "iss": api_key
    }
    payload = {
        "exp": int(time.time()) + time_valid,
    }
    token = jwt.encode(payload, api_secret, 'HS256', headers)
    return token

def consulta_estado_comprobante(URL_GRAL,request_id,api_key,api_secret):
    url = URL_GRAL 
    token = generate_token(api_key,api_secret,100000)
    data={
        "method":"Log.get_log",
        "kwargs":{"id":request_id}
    }
    os.system("echo '%s'"%(data))
    headers = { 
        "Content-Type": "application/json",
        "Authorization" : token
    }
    r = requests.post(url, headers=headers, data=json.dumps(data))

    return r

def enviar_doc_resumen_url(URL_GRAL,data_doc,token,tipoEnvio):
    url = URL_GRAL 
    data_doc["tipoEnvio"] = int(tipoEnvio)
    data={
        "method":"Factura.resumen",
        "kwargs":{"data":data_doc}
    }
    os.system("echo '%s'"%(data))
    headers = { 
        "Content-Type": "application/json",
        "Authorization" : token
    }
    r = requests.post(url, headers=headers, data=json.dumps(data))

    return r
def enviar_doc_url(URL_GRAL,data_doc,token,tipoEnvio):
    url = URL_GRAL 
    data_doc["tipoEnvio"] = int(tipoEnvio)
    data={
        "method":"Factura.lamdba",
        "kwargs":{"data":data_doc}
    }
    os.system("echo '%s'"%(data))
    headers = { 
        "Content-Type": "application/json",
        "Authorization" : token
    }
    r = requests.post(url, headers=headers, data=json.dumps(data))

    return r


def enviar_doc(self,url):
    token = generate_token(self.company_id.api_key, self.company_id.api_secret,100000)

    if self.invoice_type_code=="01" or self.invoice_type_code=="03":
        data_doc = crear_json_fac_bol(self)
    elif self.invoice_type_code=="07" or self.invoice_type_code=="08":
        data_doc = crear_json_not_cred_deb(self)
    else:
        raise UserError("Tipo de documento no valido")

    response_env = enviar_doc_url(url,data_doc, token,self.company_id.tipo_envio)

    self.json_comprobante = json.dumps(data_doc, indent=4) 
    self.json_respuesta = json.dumps(response_env.json(), indent=4)

    response_env = response_env.json()
    
    os.system("echo '%s'"%(json.dumps(response_env)))
    
    if "result" in response_env:
        if "data" in response_env["result"]:
            if "digestValue" in response_env["result"]["data"]:
                self.digest_value = response_env["result"]["data"]["digestValue"]
            self.estado_emision = extaer_estado_emision(response_env)
            self.status_envio = True
            return True, ""
        else:
            os.system("echo '%s'"%(response_env))
            recepcionado, estado_emision, msg_error = extraer_error(response_env)
            if recepcionado:
                self.status_envio = True
                self.estado_emision = estado_emision
                return True, msg_error
            else:
                return False, msg_error
    else:
        raise UserError(json.dumps(response_env))

        
def baja_doc(self):
    token = generate_token(self.company_id.api_key, self.company_id.api_secret,100000)
    data_doc = crear_json_baja(self)
    response_env = enviar_doc_resumen_url(self.company_id.endpoint,data_doc,token,self.company_id.tipo_envio)
    self.json_comprobante = data_doc
    self.json_respuesta = json.dumps(response_env.json(), indent=4)
    if response_env.ok:
        self.status_envio=True
        self.estado_emision = extaer_estado_emision(response_env.json())
        return True, ""
    else:
        recepcionado, estado_emision, msg_error = extraer_error(response_env)
        if recepcionado:
            self.status_envio = True
            self.estado_emision = estado_emision
            return True, msg_error
        else:
            return False, msg_error

def extaer_estado_emision(response_env):
    os.system("echo '%s'"%(response_env))
    result = response_env.get("result",False)
    if result:
        if result.get("data",False):
            data = result["data"]
            return data['estadoEmision']
    return ""
    

def extraer_error(response_env):
    errors = response_env["result"]['errors']
    msg_error = ""
    i_error = 1
    estado_emision = ""
    recepcionado = False
    for error in errors:
        if 'meta' in error:
            error_meta = error['meta']
            if 'estadoEmision' in error_meta:
                estado_emision = error_meta['estadoEmision']
                recepcionado = True

            if 'codigoErrorSUNAT' in error_meta:
                msg_error = msg_error + "ERROR N " + str(i_error) + ": Error en SUNAT " #+ error_meta['codigoErrorSUNAT'].encode('latin1')
            else:
                msg_error = msg_error + "ERROR N " + str(i_error) + ": " + str(error['code']) #+ " - " + error['detail'].encode('latin1')
        else:
            msg_error = msg_error + "ERROR N " + str(i_error) + ": " + str(error['code']) #+ " - " + error['detail'].encode('latin1')

        i_error = i_error + 1

    return recepcionado, estado_emision, msg_error


def get_tipo_cambio(self, compra_o_venta=2): #1 -> compra , 2->venta
    ratios = self.currency_id.rate_ids
    tipo_cambio = 1.0
    ratio_actual = False
    for ratio in ratios:
        if str(ratio.name)[0:10] == str(self.date_invoice):
            tipo_cambio = ratio.rate
            ratio_actual = True

    if ratio_actual:
        return tipo_cambio
    else:
        now = datetime.datetime.now()
        if self.date_invoice > now.strftime("%Y-%m-%d"):
            raise ValidationError("Fecha de factura no valida, no se puede obtener tipo de cambio de esa fecha")
        url = "https://www.sbs.gob.pe/app/pp/SISTIP_PORTAL/Paginas/Publicacion/TipoCambioPromedio.aspx"
        r = requests.get(url)
        if r.ok:
            soup = BeautifulSoup(r.text, 'html.parser')
            tipo_cambio = float(soup.find(id="ctl00_cphContent_rgTipoCambio_ctl00__0").find_all('td')[compra_o_venta].string)
            self.env['res.currency.rate'].create({
                'name': now.strftime("%Y-%m-%d"),
                'currency_id': self.currency_id.id,
                'rate': tipo_cambio
            })
            return tipo_cambio
        else:
            raise ValidationError("Error al obtener tipo de cambio en SBS")

#CREAR TIPOS DE JSON
def crear_json_fac_bol(self):
    now = datetime.datetime.now()
    if self.invoice_type_code == '01':
        if not re.match("^F\w{3}-\d{1,8}$",self.number):
            raise UserError("El Formato de la Factura es incorrecto.") 
    elif self.invoice_type_code == '03':
        if not re.match("^B\w{3}-\d{1,8}$",self.number):
            raise UserError("El Formato de la Boleta es Incorrecto.")
    elif self.invoice_type_code == '07':
        if self.refund_invoice_id.invoice_type_code == '01':
            if not re.match("^F\w{3}-\d{1,8}$",self.number):
                raise UserError("El Formato de la Nota de Crédito para la factura es incorrecto. ") 
        if self.refund_invoice_id.invoice_type_code == '03':
            if not re.match("^B\w{3}-\d{1,8}$",self.number):
                raise UserError("El Formato de la Nota de Crédito para la Boleta es Incorrecto. ")
    elif self.invoice_type_code == '08':
        if self.refund_invoice_id.invoice_type_code == '01':
            if not re.match("^F\w{3}-\d{1,8}$",self.number):
                raise UserError("El Formato de la Nota de Débito para la factura es incorrecto. ") 
        if self.refund_invoice_id.invoice_type_code == '03':
            if not re.match("^B\w{3}-\d{1,8}$",self.number):
                raise UserError("El Formato de la Nota de Débito para la Boleta es Incorrecto. ")
    else:
        raise UserError("El Tipo de Documento del Comprobante es Obligatorio")

    data = {
        "tipoDocumento": self.invoice_type_code,
        "fechaEmision": self.date_invoice,
        "idTransaccion": self.number,
        "correoReceptor": replace_false(self.partner_id.email if self.partner_id.email else "-"),
        "documento": {
            "serie" : self.journal_id.code,
            "correlativo" : self.journal_id.sequence_id.number_next_actual - 1,
            "nombreEmisor" : self.company_id.partner_id.registration_name,
            "tipoDocEmisor" : self.company_id.partner_id.catalog_06_id.code,
            "numDocEmisor" : self.company_id.partner_id.vat,
            "direccionReceptor": self.partner_id.street if self.partner_id.street else "-",
            "direccionOrigen" : replace_false(self.company_id.partner_id.street),
            "direccionUbigeo" : replace_false(self.company_id.partner_id.zip),
            "nombreComercialEmisor" : replace_false(self.company_id.partner_id.registration_name),
            "tipoDocReceptor" : self.partner_id.catalog_06_id.code,
            "numDocReceptor" :  self.partner_id.vat if self.partner_id.catalog_06_id.code in ["1","6"] and self.partner_id.vat  else  "-",
            "nombreReceptor" : (self.partner_id.registration_name if self.partner_id.registration_name else self.partner_id.name),
            "nombreComercialReceptor" : replace_false(self.partner_id.name if self.partner_id.name else self.partner_id.registration_name),
            #VERIFICAR
            "tipoDocReceptorAsociado" : replace_false(self.partner_id.catalog_06_id.code),
            "numDocReceptorAsociado" : self.partner_id.vat if self.partner_id.catalog_06_id.code in ["1","6"] and self.partner_id.vat  else  "-",
            "nombreReceptorAsociado" : replace_false(self.partner_id.registration_name if self.partner_id.registration_name else self.partner_id.name),
            #"direccionDestino" : "",#solo para boletas
            "tipoMoneda": self.currency_id.name,
            #"sustento" : "", #solo notas
            #"tipoMotivoNotaModificatoria" : "", #solo_notas
            "mntNeto" : round(self.total_venta_gravado, 2),
            "mntExe" : round(self.total_venta_inafecto, 2),
            "mntExo" : round(self.total_venta_exonerada, 2),
            "mntTotalIgv" : round(self.amount_tax, 2),
            "mntTotal" : round(self.amount_total, 2),
            "mntTotalGrat" : round(self.total_venta_gratuito, 2), #solo para facturas y boletas
            "fechaVencimiento" : self.date_due if self.date_due else now.strftime("%Y-%m-%d"),
            "glosaDocumento" : "VENTA", #verificar
            "codContrato" : replace_false(self.number),
            #"codCentroCosto" : "",
            "tipoCambioDestino" : round(self.tipo_cambio_fecha_factura, 4), #verificar
            "mntTotalIsc" : 0.0,
            "mntTotalOtros" : 0.0,
            "mntTotalOtrosCargos" : 0.0,
            #"mntTotalAnticipos" : 0.0, #solo factura y boleta
            "tipoFormatoRepresentacionImpresa" : "GENERAL",
            "mntTotalLetras":to_word(round(self.amount_total, 2),self.currency_id.name) 
        },
        "descuento": {
            "mntDescuentoGlobal" : self.total_descuento_global,
            "mntTotalDescuentos" : self.total_descuentos
        },
        #solo factura y boleta
        #"servicioHospedaje": { },
        # solo factura y boleta, con expecciones

        "indicadores": {
            #VERIFICAR ESTOS CAMPOS
            "indVentaInterna":True if self.tipo_operacion == "01" else 0,
            "indExportacion" : True if self.tipo_operacion == "02" else 0,
            #"indNoDomiciliados" : False, #valido para notas
            "indAnticipo" : True if self.tipo_operacion == "04" else 0,
            #"indDeduccionAnticipos" : False,
            #"indServiciosHospedaje" : False,
            "indVentaItinerante" : True if self.tipo_operacion == "05" else 0
            #"indTrasladoBienesConRepresentacionImpresa" : False,
            #"indVentaArrozPilado" : False,
            #"indComprobantePercepcion" : False,
            #"indBienesTransferidosAmazonia" : False,
            #"indServiciosPrestadosAmazonia" : False,
            #"indContratosConstruccionEjecutadosAmazonia" : False

        },

        #solo factura y boleta
        #"percepcion": {
            #"mntBaseImponible" : 0.0,
            #"mntPercepcion" : 0.0,
            #"mntTotalMasPercepcion" : 0.0,
            #"tasaPercepcion" : 1.0
        #},
        #"datosAdicionales": {},
    }
    data_impuesto = []
    data_detalle = []
    data_referencia = [] #solo para notas
    data_anticipo = [] #solo facturas y boletas
    data_anexo = [] #si hay anexos

    for tax in self.tax_line_ids:
        data_impuesto.append({
            "codImpuesto": str(tax.tax_id.tax_group_id.code),
            "montoImpuesto":tax.amount,
            "tasaImpuesto": tax.tax_id.amount/100
        })
    
    if len(self.tax_line_ids)==0:
        data_impuesto.append({
            "codImpuesto": "1000",
            "montoImpuesto":0.0,
            "tasaImpuesto": 0.18
        })

    for item in self.invoice_line_ids:
        #price_unit = item.price_unit*(1-(item.discount/100)) - item.descuento_unitario 
        #descuento = item.price_unit*item.discount/100 + item.descuento_unitario 
        price_unit = item.price_unit
        descuento_unitario = item.descuento_unitario
        descuento = 0
        tasaIgv = item.invoice_line_tax_ids[0].amount/100 if len(item.invoice_line_tax_ids) else ""
        if (item.invoice_line_tax_ids.price_include):
            
            if (item.invoice_line_tax_ids.amount == 0):
                montoImpuestoUni = 0
                base_imponible = price_unit
                descuento = (base_imponible*item.discount/100 +  descuento_unitario)
            else:
                base_imponible = price_unit / (1+tasaIgv)
                descuento_unitario = descuento_unitario / (1+tasaIgv)
                descuento = (base_imponible*item.discount/100 +  descuento_unitario)
                montoImpuestoUni = price_unit - base_imponible - descuento*tasaIgv
            precioItem = price_unit
        else:
            base_imponible = price_unit
            descuento = (base_imponible*item.discount/100 +  descuento_unitario)
            montoImpuestoUni =(price_unit -descuento)*tasaIgv 
            precioItem = price_unit + montoImpuestoUni
            base_imponible = price_unit

       
        datac={
            "cantidadItem" : round(item.quantity, 4),
            "unidadMedidaItem" : item.uom_id.code,
            "codItem" : str(item.product_id.id),
            "nombreItem" : item.name,
            "precioItem" : round(precioItem, 4),
            "precioItemSinIgv" : round(base_imponible, 4),
            "montoItem" : round((base_imponible-descuento) * item.quantity, 4),
            "descuentoMonto" : round(descuento, 4), #solo factura y boleta
            "codAfectacionIgv" : item.invoice_line_tax_ids[0].tipo_afectacion_igv.code if len(item.invoice_line_tax_ids) else "",
            "tasaIgv" : tasaIgv,
            "montoIgv" : round(montoImpuestoUni * item.quantity, 4),
            "codSistemaCalculoIsc" : "01", #VERIFICAR
            "montoIsc" : 0.0, #VERIFICAR
            #"tasaIsc" : 0.0, #VERIFICAR
            "precioItemReferencia" : item.product_id.lst_price, #VERIFICAR
            "idOperacion" : str(self.id),
            "no_onerosa": True if item.no_onerosa else False
        }

        data_detalle.append(datac)
        

    data["impuesto"] = data_impuesto
    data["detalle"] = data_detalle
    if len(data_anticipo):
        data["anticipos"] = data_anticipo
    if len(data_anexo):
        data["anexos"] = data_anexo

    return data
    #return json.dumps(data,indent=4)

def crear_json_not_cred_deb(self):
    now = datetime.datetime.now()
    data = {
        "tipoDocumento": self.invoice_type_code,
        "fechaEmision": self.date_invoice,
        "idTransaccion": self.number,
        "correoReceptor": replace_false(self.partner_id.email if self.partner_id.email else "-"),
        "documento": {
            "serie": self.journal_id.code,
            "correlativo": self.journal_id.sequence_id.number_next_actual - 1,
            "nombreEmisor": self.company_id.partner_id.registration_name,
            "tipoDocEmisor": self.company_id.partner_id.catalog_06_id.code,
            "numDocEmisor": self.company_id.partner_id.vat,
            "direccionOrigen": replace_false(self.company_id.partner_id.street),
            "direccionUbigeo": replace_false(self.company_id.partner_id.zip),
            "nombreComercialEmisor": replace_false(self.company_id.partner_id.registration_name),
            "tipoDocReceptor": self.partner_id.catalog_06_id.code,
            "numDocReceptor": self.partner_id.vat,
            "nombreReceptor": (
                self.partner_id.registration_name if self.partner_id.registration_name else self.partner_id.name),
            "nombreComercialReceptor": replace_false(
                self.partner_id.name if self.partner_id.name else self.partner_id.registration_name),

            # VERIFICAR
            "tipoDocReceptorAsociado": replace_false(self.partner_id.catalog_06_id.code),
            "numDocReceptorAsociado": replace_false(self.partner_id.vat),
            "nombreReceptorAsociado": replace_false(
                self.partner_id.registration_name if self.partner_id.registration_name else self.partner_id.name),

            # "direccionDestino" : "",#solo para boletas
            "tipoMoneda": self.currency_id.name,
            "sustento" : replace_false(self.sustento_nota), #solo notas
            "tipoMotivoNotaModificatoria" : str(self.tipo_nota_credito.code if self.invoice_type_code== "07" else self.tipo_nota_dedito.code), #solo_notas
            "mntNeto": round(self.total_venta_gravado, 2),
            "mntExe": round(self.total_venta_inafecto, 2),
            "mntExo": round(self.total_venta_exonerada, 2),
            "mntTotalIgv": round(self.amount_tax, 2),
            "mntTotal": round(self.amount_total, 2),
            #"mntTotalGrat": round(self.total_venta_gratuito, 2),  # solo para facturas y boletas
            "fechaVencimiento": self.date_due if self.date_due else now.strftime("%Y-%m-%d"),
            "glosaDocumento": "VENTA",  # verificar
            "codContrato": replace_false(self.number),
            # "codCentroCosto" : "",
            "tipoCambioDestino":round(self.tipo_cambio_fecha_factura, 4),  # verificar
            "mntTotalIsc": 0.0,
            "mntTotalOtros": 0.0,
            "mntTotalOtrosCargos": 0.0,
            # "mntTotalAnticipos" : 0.0, #solo factura y boleta
            "tipoFormatoRepresentacionImpresa": "GENERAL",
            

        },
        "descuento": {
            #"mntDescuentoGlobal": self.total_descuento_global,
            "mntTotalDescuentos": self.total_descuentos
        },
        # solo factura y boleta
        # "servicioHospedaje": { },
        # solo factura y boleta, con expecciones

        "indicadores": {
            # VERIFICAR ESTOS CAMPOS

            # "indExportacion" : False,
            # "indNoDomiciliados" : False, #valido para notas
            # "indAnticipo" : True,
            # "indDeduccionAnticipos" : False,
            # "indServiciosHospedaje" : False,
            # "indVentaItinerante" : False,
            # "indTrasladoBienesConRepresentacionImpresa" : False,
            # "indVentaArrozPilado" : False,
            # "indComprobantePercepcion" : False,
            # "indBienesTransferidosAmazonia" : False,
            # "indServiciosPrestadosAmazonia" : False,
            # "indContratosConstruccionEjecutadosAmazonia" : False

        },

        # solo factura y boleta
        # "percepcion": {
        # "mntBaseImponible" : 0.0,
        # "mntPercepcion" : 0.0,
        # "mntTotalMasPercepcion" : 0.0,
        # "tasaPercepcion" : 1.0
        # },
        # "datosAdicionales": {},
    }
    data_impuesto = []
    data_detalle = []
    data_referencia = []  # solo para notas
    data_anticipo = []  # solo facturas y boletas
    data_anexo = []  # si hay anexos

    for tax in self.tax_line_ids:
        data_impuesto.append({
            "codImpuesto": str(tax.tax_id.tax_group_id.code),
            "montoImpuesto":tax.base,
            "tasaImpuesto": tax.tax_id.amount/100
        })
    
    if len(self.tax_line_ids)==0:
        data_impuesto.append({
            "codImpuesto": "1000",
            "montoImpuesto":0.0,
            "tasaImpuesto": 0.18
        })

    for item in self.invoice_line_ids:
        price_unit = item.price_unit*(1-(item.discount/100)) - item.descuento_unitario 
        if (item.invoice_line_tax_ids.price_include):

            if (item.invoice_line_tax_ids.amount == 0):
                montoImpuestoUni = 0
                base_imponible = price_unit
            else:
                base_imponible = price_unit / (1 + (item.invoice_line_tax_ids.amount / 100))
                montoImpuestoUni = price_unit - base_imponible

            precioItem = price_unit

        else:
            montoImpuestoUni = price_unit * (item.invoice_line_tax_ids.amount / 100)

            precioItem = price_unit + montoImpuestoUni
            base_imponible = price_unit

        '''
        data_impuesto.append({
            "codImpuesto": str(item.invoice_line_tax_ids.tax_group_id.code),
            "montoImpuesto": round(montoImpuestoUni * item.quantity, 2),
            "tasaImpuesto": round(item.invoice_line_tax_ids.amount / 100, 2)
        })
        
        '''
        data_detalle.append({
            "cantidadItem": round(item.quantity, 3),
            "unidadMedidaItem": item.uom_id.code,
            "codItem": str(item.product_id.id),
            "nombreItem": item.product_id.name,
            "precioItem": round(precioItem, 2),
            "precioItemSinIgv": round(base_imponible, 2),
            "montoItem": round(precioItem * item.quantity, 2),
            #"descuentoMonto": item.discount * precioItem / 100,  # solo factura y boleta
            "codAfectacionIgv":  item.invoice_line_tax_ids[0].tipo_afectacion_igv.code if len(item.invoice_line_tax_ids)>0 else False,
            "tasaIgv": item.invoice_line_tax_ids.amount / 100,
            "montoIgv": round(montoImpuestoUni * item.quantity, 2),
            "codSistemaCalculoIsc": "01",  # VERIFICAR
            "montoIsc": 0.0,  # VERIFICAR
            # "tasaIsc" : 0.0, #VERIFICAR
            "precioItemReferencia" : item.product_id.lst_price,
            "idOperacion": str(self.id),
            "no_onerosa": True if item.no_onerosa else False
        })
    document_reference = self.refund_invoice_id
    data_referencia.append({
        'tipoDocumentoRef': document_reference.invoice_type_code,
        'serieRef': document_reference.number[0:4],
        'correlativoRef': int(document_reference.number[5:len(document_reference.number)]),
        'fechaEmisionRef': document_reference.date_due
    })
    data["impuesto"] = data_impuesto
    data["detalle"] = data_detalle
    data["anticipos"] = data_anticipo
    data["anexos"] = data_anexo
    data["referencia"] = data_referencia

    return data

def crear_json_baja(self,tipo_resumen="RA"):
    now = datetime.datetime.now()
    data = {
        "tipoResumen": tipo_resumen,
        "fechaGeneracion": self.date_invoice,
        "idTransaccion": str(self.id),
        "resumen": {
            "id": self.contador,
            "tipoDocEmisor": self.company_id.partner_id.catalog_06_id.code,
            "numDocEmisor": self.company_id.partner_id.vat,
            "nombreEmisor": self.company_id.partner_id.registration_name,
            "fechaReferente": self.date_invoice,
            "tipoFormatoRepresentacionImpresa": "GENERAL"
        }
    }

    data_detalle = []
    for document in self.invoice_ids:
        data_detalle.append({
            "serie": document.number[0:4],
            "correlativo": int(document.number[5:len(document.number)]),
            "tipoDocumento": document.invoice_type_code,
            "motivo": self.motivo
        })

    data['detalle'] = data_detalle

    return data
    #return json.dumps(data, indent=4)

def replace_false(dato):
    if dato:
        return dato
    else:
        return ""



        