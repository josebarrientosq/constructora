# -*- coding: utf-8 -*-

from datetime import date

from odoo import models, fields, api, _
from odoo.exceptions import Warning

class MrpBom(models.Model):
    _inherit = 'mrp.bom' #odoo11

    material_total = fields.Float(  string='Total costo Material', compute='_compute_material_total', store=True )
    labor_total = fields.Float(string='Total costo labor', compute='_compute_labor_total', store=True )
    overhead_total = fields.Float( string='Total costo Maquinaria',compute='_compute_overhead_total',store=True )
    jobcost_total = fields.Float( string='Total Costo', compute='_compute_jobcost_total',store=True)

    @api.depends('bom_line_ids')
    def _compute_material_total(self):
        for rec in self:
            for p in rec.bom_line_ids:
                if (p.boq_tipo == 'MA'):
                    rec.material_total += p.product_qty * p.cost_price

    @api.depends('bom_line_ids')
    def _compute_labor_total(self):
        self.labor_total=0
        for p in self.bom_line_ids:
            if (p.boq_tipo == 'MO'):
                self.labor_total += p.product_qty * p.cost_price

    @api.depends('bom_line_ids')
    def _compute_overhead_total(self):
        for rec in self:
            for p in rec.bom_line_ids:
                if (p.boq_tipo == 'MH'):
                    rec.overhead_total += p.product_qty * p.cost_price

    @api.depends('bom_line_ids')
    def _compute_jobcost_total(self):
        for rec in self:
            rec.jobcost_total = rec.material_total + rec.labor_total + rec.overhead_total


class MrpBomLine(models.Model):
    _inherit = 'mrp.bom.line'


    description = fields.Char( string='Description',copy=False )
    cost_price = fields.Float( string='Cost / Unit',copy=False )
    total_cost = fields.Float(string='Cost Price Sub Total', compute='_compute_total_cost', store=True )
    boq_tipo = fields.Char(string='Tipo')
    analytic_id = fields.Many2one( 'account.analytic.account', string='Analytic Account' )
    currency_id = fields.Many2one('res.currency', string='Currency', default=lambda self: self.env.user.company_id.currency_id, readonly=True )

    @api.onchange('product_id')
    def _onchange_product_id(self):
        for rec in self:
            rec.description = rec.product_id.name
            rec.product_qty = 1.0
            rec.uom_id = rec.product_id.uom_id.id
            rec.cost_price = rec.product_id.standard_price  # lst_price
            rec.boq_tipo = rec.product_id.boq_tipo

    @api.depends('product_qty', 'cost_price')
    def _compute_total_cost(self):
        for rec in self:
            #if rec.job_type == 'labour':
            #    rec.product_qty = 0.0
            #    rec.total_cost = rec.hours * rec.cost_price
            #else:
            #    rec.hours = 0.0
            rec.total_cost = rec.product_qty * rec.cost_price
