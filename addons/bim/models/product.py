# -*- coding: utf-8 -*-

from odoo import models, fields

class Product(models.Model):
    _inherit = 'product.template'
    
    boq_tipo = fields.Selection([
        ('MA', 'Material'),
        ('MO', 'Mano de obra'),
        ('MH', 'Maquinaria'),
        ('otro', 'Otros')],
        string='Tipo',
    )
